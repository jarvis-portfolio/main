﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cre8NCutzApp.Models
{
	internal class TwilioConfig
	{
		public string AccountSID { get; set; }
		public string AuthToken { get; set; }
	}
}
