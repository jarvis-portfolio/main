﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Twilio.Rest.Api.V2010.Account;

namespace Cre8NCutzApp.Models
{
	public class AppointmentModel
	{
		#region ctors
		public AppointmentModel()
		{

		}
		/// <summary>
		/// Database Model for Appointments
		/// </summary>
		#endregion

		#region fields

		private int _ApptID = 0;
		private int _CustID = 0;
		private int _UserID = 0;
		private string _ApptTitle = null;
		private string _ApptDescription = null;
		private string _ApptLocation = null;
		private string _ApptContact = null;
		private string _ApptType = null;
		private string _ApptURL = null;
		private DateTime _ApptStartTime;
		private DateTime _ApptEndTime;
		private DateTime _ApptCreateDate;
		private string _ApptCreatedBy = null;
		private DateTime _ApptLastUpdateDate;
		private string _ApptLastUpdatedBy = null;
		private string _CustName;
		public string twilioNo = "+19786620952";

		#endregion

		#region properties
		public int ApptID
		{
			get
			{
				return _ApptID;
			}
			set
			{
				_ApptID = value;
			}
		}
		public int CustId
		{
			get
			{
				return _CustID;
			}
			set
			{
				_CustID = value;
			}
		}

		public string CustName
		{
			get { return _CustName; }
			set { _CustName = value; }
		}

		public int UserID
		{
			get
			{
				return _UserID;
			}
			set
			{
				_UserID = value;
			}
		}
		public string ApptTitle
		{
			get
			{
				return _ApptTitle;
			}
			set
			{
				_ApptTitle = value;
			}
		}
		public string ApptDescription
		{
			get
			{
				return _ApptDescription;
			}
			set
			{
				_ApptDescription = value;
			}
		}
		public string ApptLocation
		{
			get
			{
				return _ApptLocation;
			}
			set
			{
				_ApptLocation = value;
			}
		}
		public string ApptContact
		{
			get
			{
				return _ApptContact;
			}
			set
			{
				_ApptContact = value;
			}
		}
		public string ApptType
		{
			get
			{
				return _ApptType;
			}
			set
			{
				_ApptType = value;
			}
		}
		public string ApptURL
		{
			get
			{
				return _ApptURL;
			}
			set
			{
				_ApptURL = value;
			}
		}
		public DateTime ApptStartTime 
		{
			get
			{
				return _ApptStartTime; 
			}
			set
			{
				_ApptStartTime = value;
			}
		}
		public DateTime ApptEndTime
		{
			get
			{
				return _ApptEndTime;
			}
			set
			{
				_ApptEndTime = value;
			}
		}
		public DateTime ApptCreateDate
		{
			get
			{
				return _ApptCreateDate;
			}
			set
			{
				_ApptCreateDate = value;
			}
		}
		public string ApptCreatedBy
		{
			get
			{
				return _ApptCreatedBy;
			}
			set
			{
				_ApptCreatedBy = value;
			}
		}
		public DateTime ApptLastUpdateDate
		{
			get
			{
				return _ApptLastUpdateDate;
			}
			set
			{
				_ApptLastUpdateDate = value;
			}
		}
		public string ApptLastUpdatedBy
		{
			get
			{
				return _ApptLastUpdatedBy;
			}
			set
			{
				_ApptLastUpdatedBy = value;
			}
		}

		#endregion

		#region cxmethods

		public void sendSMSApptNotify(AppointmentModel appt)
		{
			
			switch (appt.ApptType)
			{
				case "Convention":
					new Convention().SMSApptNotify(appt);
					break;
				case "Deadline":
					new Deadline().SMSApptNotify(appt);
					break;
				case "Retrospective":
					new Retrospective().SMSApptNotify(appt);
					break;
				case "Shipment-In":
					new ShipmentIn().SMSApptNotify(appt);
					break;
				case "Shipment-Out":
					new ShipmentOut().SMSApptNotify(appt);
					break;
				case "Pre-Order":
					new PreOrder().SMSApptNotify(appt);
					break;
				case "Market":
					new Market().SMSApptNotify(appt);
					break;

			}
		}
		
		public virtual void SMSApptNotify(AppointmentModel appt)
		{
			//if (apptStart == "") //specific ApptTime then 
			//{ }
		}

		#endregion
	}
	public class Convention : AppointmentModel
	{

		#region cxmethods
		public override void SMSApptNotify(AppointmentModel appt)
		{

			// Set sms message to display
			// "This is a message to notify you that you need to leave for the 'Convention' 'Akon:2023'"
			//var smsNotify = MessageResource.Create(body: $"This is a message to notify you that you need to leave for the {appt.ApptType} at {appt.CustName}.", from: new Twilio.Types.PhoneNumber(twilioNo), to: new Twilio.Types.PhoneNumber(""));
			MessageBox.Show($"(SMS:)This is a message to notify you that you need to leave for the {appt.ApptType} at {appt.CustName}.");

			//base.SMSApptNotify();
		}
		#endregion
	}

	public class Deadline : AppointmentModel
	{

		#region cxmethods
		public override void SMSApptNotify(AppointmentModel appt)
		{
			//var smsNotify = MessageResource.Create(body: $"This is a message to notify you that your {appt.ApptType} for {appt.CustName} is due today.", from: new Twilio.Types.PhoneNumber(twilioNo), to: new Twilio.Types.PhoneNumber(""));
			// Set sms message to display
			// "This is a message to notify you that your 'Deadline' for 'John Doe' is due today."
			MessageBox.Show($"(SMS:)This is a message to notify you that your {appt.ApptType} for {appt.CustName} is due today.");

		}
		#endregion

	}

	public class Retrospective : AppointmentModel
	{

		#region cxmethods
		public override void SMSApptNotify(AppointmentModel appt)
		{

			// Set sms message to display
			// "This is your reminder to hold your 'Retrospective' review for 'Akon:2023'".
			//var smsNotify = MessageResource.Create(body: $"This is your reminder to hold your {appt.ApptType} review for {appt.CustName}.", from: new Twilio.Types.PhoneNumber(twilioNo), to: new Twilio.Types.PhoneNumber(""));
			MessageBox.Show($"(SMS:)This is your reminder to hold your {appt.ApptType} review for {appt.CustName}.");
						
		}
		#endregion

	}

	public class ShipmentIn : AppointmentModel
	{

		#region cxmethods
		public override void SMSApptNotify(AppointmentModel appt)
		{
			// Set sms message to display
			// "This is a reminder that your 'Shipment-In' from 'Ms.Fabrics' is coming in today". 
			//var smsNotify = MessageResource.Create(body: $"This is a reminder that your {appt.ApptType} from {appt.CustName} is coming in today.", from: new Twilio.Types.PhoneNumber(twilioNo), to: new Twilio.Types.PhoneNumber(""));
			MessageBox.Show($"(SMS:)This is a reminder that your {appt.ApptType} from {appt.CustName} is coming in today.");
			
		}
		#endregion

	}

	public class ShipmentOut : AppointmentModel
	{

		#region cxmethods
		public override void SMSApptNotify(AppointmentModel appt)
		{

			// Set sms message to display
			// "This is a reminder that you need to send your 'Shipment-Out' for 'John Doe'".
			//var smsNotify = MessageResource.Create(body: $"This is a reminder that you need to send your {appt.ApptType} for {appt.CustName}.", from: new Twilio.Types.PhoneNumber(twilioNo), to: new Twilio.Types.PhoneNumber(""));
			MessageBox.Show($"(SMS:)This is a reminder that you need to send your {appt.ApptType} for {appt.CustName}.");

		}
		#endregion

	}

	public class PreOrder : AppointmentModel
	{

		#region cxmethods
		public override void SMSApptNotify(AppointmentModel appt)
		{

			// Set sms message to display
			// "This is a reminder that the 'Pre-Order' for 'Ms.Fabrics'
			// is opening today". *30  mins before*
			//var smsNotify = MessageResource.Create(body: $"This is a reminder that the {appt.ApptType} for {appt.CustName} is opening today.", from: new Twilio.Types.PhoneNumber(twilioNo), to: new Twilio.Types.PhoneNumber(""));
			MessageBox.Show($"(SMS:)This is a reminder that the {appt.ApptType} for {appt.CustName} is opening today.");

		}
		#endregion

	}

	public class Market : AppointmentModel
	{

		#region cxmethods
		public override void SMSApptNotify(AppointmentModel appt)
		{
			// Set sms message to display
			// "This is a message to notify you that you need to leave for the 'Market' 'Holiday Bazaar'".
			//var smsNotify = MessageResource.Create(body: $"This is a message to notify you that you need to leave for the {appt.ApptType}, {appt.CustName} ASAP.", from: new Twilio.Types.PhoneNumber(twilioNo), to: new Twilio.Types.PhoneNumber(""));
			MessageBox.Show($"(SMS:)This is a message to notify you that you need to leave for the {appt.ApptType}, {appt.CustName} ASAP.");

		}
		#endregion

	}
}
