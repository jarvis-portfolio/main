﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cre8NCutzApp.Models
{
	public class AddressModel
	{
		#region ctors
		public AddressModel()
		{

		}
		/// <summary>
		/// Database Model for Address
		/// </summary>
		
		public AddressModel(int addrID, string addrStreet, string addrStreet2, int addrCityID, string addrPostal,
			int addrPhone, DateTime addrCreateDate, string addrCreatedBy, DateTime addrLastUpdateTime, string addrLastUpdatedBy)
		{
			AddrID = addrID;
			AddrStreet = addrStreet;
			AddrStreet2 = addrStreet2;
			AddrCityID = addrCityID;
			AddrPostal = addrPostal;
			AddrPhone = addrPhone;
			AddrCreateDate = addrCreateDate;
			AddrCreatedBy = addrCreatedBy;
			AddrLastUpdateTime = addrLastUpdateTime;
			AddrLastUpdatedBy = addrLastUpdatedBy;
		}
		#endregion

		#region fields

		private int _AddrID = 0;
		private string _AddrStreet = null;
		private string _AddrStreet2 = null;
		private int _AddrCityID = 0;
		private string _AddrPostal = null;
		private int _AddrPhone = 0;
		private DateTime _AddrCreateDate;
		private string _AddrCreatedBy = null;
		private DateTime _AddrLastUpdateTime;
		private string _AddrLastUpdatedBy = null;

		#endregion

		#region properties
		public int AddrID
		{
			get
			{
				return _AddrID;
			}
			set
			{
				_AddrID = value;
			}
		}
		public string AddrStreet
		{
			get
			{
				return _AddrStreet;
			}
			set
			{
				_AddrStreet = value;
			}
		}
		public string AddrStreet2
		{
			get
			{
				return _AddrStreet2;
			}
			set
			{
				_AddrStreet2 = value;
			}
		}
		public int AddrCityID
		{
			get
			{
				return _AddrCityID;
			}
			set
			{
				_AddrCityID = value;
			}
		}
		public string AddrPostal
		{
			get
			{
				return _AddrPostal;
			}
			set
			{
				_AddrPostal = value;
			}
		}
		public int AddrPhone
		{
			get
			{
				return _AddrPhone;
			}
			set
			{
				_AddrPhone = value;
			}
		}
		public DateTime AddrCreateDate
		{
			get 
			{ 
				return _AddrCreateDate; 
			}
			set 
			{ 
				_AddrCreateDate = value; 
			}
		}
		public string AddrCreatedBy
		{
			get
			{
				return _AddrCreatedBy;
			}
			set
			{
				_AddrCreatedBy = value;
			}
		}
		public DateTime AddrLastUpdateTime
		{
			get
			{
				return _AddrLastUpdateTime;
			}
			set
			{
				_AddrLastUpdateTime = value;
			}
		}
		public string AddrLastUpdatedBy
		{
			get
			{
				return _AddrLastUpdatedBy;
			}
			set
			{
				_AddrLastUpdatedBy = value;
			}
		}

		#endregion
	}
}
