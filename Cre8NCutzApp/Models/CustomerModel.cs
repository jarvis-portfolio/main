﻿using Cre8NCutzApp.Controllers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cre8NCutzApp.Models
{
	public class CustomerModel
	{ 
		// Data comes from the Database which the methods are all in DBConnect class

		#region ctors
		public CustomerModel()
		{

		}

		/// <summary>
		/// Database Model for Customer
		/// </summary>
		
		

		public CustomerModel(string cxName, string cxAdd, string cxPhone,string cxCity, string cxCountry)
		{
			
			CustName = cxName;
			CustAddr = cxAdd;
			CustAddrPhone = cxPhone;
			CustCityName = cxCity;
			CustCountryName = cxCountry;
		}

		#endregion

		#region fields
		DBConnect dbConnect = new DBConnect();
		AddressModel addrModel = new AddressModel();
		CityModel cityModel = new CityModel();
		CountryModel countryModel = new CountryModel();
		private int _CustId = 0;
		private string _CustName = null;
		private int _CustAddrId = 0;
		private string _CustAddr = null;
		private string _CustAddrPhone = null;
		private int _CustCityId = 0;
		private string _CustCityName = null;
		private int _CustCountryId = 0;
		private string _CustCountryName = null;
		
		private BindingSource _CustBindingSource = null;
		private BindingList<CustomerModel> _DBCust = new BindingList<CustomerModel>();

		#endregion

		#region properties

		public int CustId
		{
			get 
			{ 
				return _CustId; 
			}
			set 
			{ 
				_CustId = value; 
			}
		}
		public string CustName
		{
			get
			{
				return _CustName;
			}
			set
			{
				_CustName = value;
			}
		}
		public int CustAddrId 
		{
			get
			{
				return _CustAddrId;
			}
			set
			{
				_CustAddrId = value;
			}
		}
		public string CustAddr 
		{
			get
			{
				return _CustAddr;
			}
			set
			{
				_CustAddr = value;
			}
		}
		public string CustAddrPhone 
		{
			get
			{
				return _CustAddrPhone;
			}
			set
			{
				_CustAddrPhone = value;
			}
		}
		public int CustCityId 
		{
			get
			{
				return _CustCityId;
			}
			set
			{
				_CustCityId = value;
			}
		}
		public string CustCityName 
		{
			get
			{
				return _CustCityName;
			}
			set
			{
				_CustCityName = value;
			}
		}
		public int CustCountryId 
		{
			get
			{
				return _CustCountryId;
			}
			set
			{
				_CustCountryId = value;
			}
		}
		public string CustCountryName 
		{
			get
			{
				return _CustCountryName;
			}
			set
			{
				_CustCountryName = value;
			}
		}

		private BindingSource _CustAddrBS;

		public BindingSource CustAddrBS
		{
			get 
			{ 
				if(_CustAddrBS == null)
				{
					_CustAddrBS = new BindingSource();
					_CustAddrBS.DataSource = CustAddr;
				}
				return _CustAddrBS; 
			}
			set { _CustAddrBS = value; }
		}

		

		public string CustInfo
		{
			get
			{
				return $"{CustId} {CustName} {CustAddr} {addrModel.AddrPhone} {cityModel.Cities} {countryModel.Countries}"; 
			}
		}
		public BindingList<CustomerModel> ListDBCust ()
		{
			throw new NotImplementedException();
		}
		public BindingSource CustBindingSource
		{
			get
			{
				if(_CustBindingSource == null)
				{
					_CustBindingSource = new BindingSource();
					_CustBindingSource.DataSource = new BindingSource();
				}
				return _CustBindingSource;
			}
			set
			{
				_CustBindingSource = value;
			}
		}

		#endregion

		

	}
}
