﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cre8NCutzApp.Models
{
	public class CountryModel
	{
		#region ctors
		public CountryModel()
		{

		}
		/// <summary>
		/// Database Model for Country
		/// </summary>
		#endregion

		#region fields

		private int _CountryID = 0;
		private string _Countries = null;
		private DateTime _CountryCreateDate;
		private string _CountryCreatedBy = null;
		private DateTime _CountryLastUpdateTime;
		private string _CountryLastUpdatedBy = null;

		#endregion

		#region properties
		public int CountryID
		{
			get
			{
				return _CountryID;
			}
			set
			{
				_CountryID = value;
			}
		}
		public string Countries
		{
			get
			{
				return _Countries;
			}
			set
			{
				_Countries = value;
			}
		}
		public DateTime CountryCreateDate
		{
			get
			{
				return _CountryCreateDate;
			}
			set
			{
				_CountryCreateDate = value;
			}
		}
		public string CountryCreatedBy
		{
			get
			{
				return _CountryCreatedBy;
			}
			set
			{
				_CountryCreatedBy = value;
			}
		}
		public DateTime CountryLastUpdateTime
		{
			get
			{
				return _CountryLastUpdateTime;
			}
			set
			{
				_CountryLastUpdateTime = value;
			}
		}
		public string CountryLastUpdatedBy
		{
			get
			{
				return _CountryLastUpdatedBy;
			}
			set
			{
				_CountryLastUpdatedBy = value;
			}
		}

		#endregion

	}
}
