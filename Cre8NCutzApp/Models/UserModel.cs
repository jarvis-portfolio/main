﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Cre8NCutzApp.Controllers;
using MySql.Data.MySqlClient;
using System.Data;

namespace Cre8NCutzApp.Models
{
	public class UserModel 
	{

		#region ctors
		public UserModel()
		{
			
			
		}
		/// <summary>
		/// Database Model for User
		/// </summary>

		#endregion

		#region methods


		#endregion


		#region fields

		private int _UserID = 0;
		private string _UserName = null;
		private string _UserPassword = null;
		private int _UserActive = 0;
		private DateTime _UserCreateDate;
		private string _UserCreatedBy = null;
		private DateTime _UserLastUpdateTime;
		private string _UserLastUpdatedBy = null;

		private string _username;
		private string _password;


		#endregion

		#region properties
		

		public string username
		{
			get { return _username = LoginScrn.InstLoginScrn.txt_Username.Text; }
			set { _username = value; }
		}
		public string password
		{
			get { return _password = LoginScrn.InstLoginScrn.txt_Password.Text; }
			set { _password = value; }
		}
		public int UserID
		{
			get
			{
				return _UserID;
			}
			set
			{
				_UserID = value;
			}
		}
		public string UserName
		{
			get
			{
				
				return _UserName;
			}
			set
			{
				_UserName = value;
			}
		}
		public string UserPassword
		{
			get
			{
				return _UserPassword;
			}
			set
			{
				_UserPassword = value;
			}
		}
		public int UserActive
		{
			get
			{
				return _UserActive;
			}
			set
			{
				_UserActive = value;
			}
		}
		public DateTime UserCreateDate
		{
			get
			{
				return _UserCreateDate;
			}
			set
			{
				_UserCreateDate = value;
			}
		}
		public string UserCreatedBy
		{
			get
			{
				return _UserCreatedBy;
			}
			set
			{
				_UserCreatedBy = value;
			}
		}
		public DateTime UserLastUpdateTime
		{
			get
			{
				return _UserLastUpdateTime;
			}
			set
			{
				_UserLastUpdateTime = value;
			}
		}
		public string UserLastUpdatedBy
		{
			get
			{
				return _UserLastUpdatedBy;
			}
			set
			{
				_UserLastUpdatedBy = value;
			}
		}

		#endregion

		#region methods
		UserModel(int userId, string userName, string userPassword, int userActive, 
			DateTime userCreateDate, string userCreatedBy, DateTime userLastUpdate, string userUpdateBy)
		{
			UserID =  userId;
			UserName = userName;
			UserPassword = userPassword;
			UserActive = userActive;
			UserCreateDate = userCreateDate;
			UserCreatedBy = userCreatedBy;
			UserLastUpdateTime = userLastUpdate;
			UserLastUpdatedBy = userUpdateBy;
		}


		#endregion
	}
}
