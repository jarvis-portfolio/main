﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cre8NCutzApp.Models
{
	class CityModel
	{
		#region ctors
		public CityModel()
		{

		}
		/// <summary>
		/// Database Model for City
		/// </summary>
		#endregion

		#region fields

		private int _CityID = 0;
		private string _Cities = null;
		private int _CountryID = 0;
		private DateTime _CityCreateDate;
		private string _CityCreatedBy = null;
		private DateTime _CityLastUpdateTime;
		private string _CityLastUpdatedBy = null;

		#endregion

		#region properties
		public int CityID
		{
			get
			{
				return _CityID;
			}
			set
			{
				_CityID = value;
			}
		}
		public string Cities
		{
			get
			{
				return _Cities;
			}
			set
			{
				_Cities = value;
			}
		}
		public int CountryID
		{
			get
			{
				return _CountryID;
			}
			set
			{
				_CountryID = value;
			}
		}
		public DateTime CityCreateDate
		{
			get
			{
				return _CityCreateDate;
			}
			set
			{
				_CityCreateDate = value;
			}
		}
		public string CityCreatedBy
		{
			get
			{
				return _CityCreatedBy;
			}
			set
			{
				_CityCreatedBy = value;
			}
		}
		public DateTime CityLastUpdateTime
		{
			get
			{
				return _CityLastUpdateTime;
			}
			set
			{
				_CityLastUpdateTime = value;
			}
		}
		public string CityLastUpdatedBy
		{
			get
			{
				return _CityLastUpdatedBy;
			}
			set
			{
				_CityLastUpdatedBy = value;
			}
		}

		#endregion

	}
}
