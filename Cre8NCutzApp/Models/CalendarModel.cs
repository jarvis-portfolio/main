﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cre8NCutzApp.Models
{
	public class CalendarModel
	{
		#region ctors
		public CalendarModel()
		{

		}
		/// <summary>
		/// Database Model for Calendar
		/// </summary>
		#endregion

		#region fields

		private DateTime _CalendarMonth;
		private DateTime _CalendarWeek;
		private DateTime _CalendarDay;

		#endregion

		#region properties
		public DateTime CalendarMonth
		{
			get
			{
				return _CalendarMonth;
			}
			set
			{
				_CalendarMonth = value;
			}
		}
		public DateTime CalendarWeek
		{
			get
			{
				return _CalendarWeek;
			}
			set
			{
				_CalendarWeek = value;
			}
		}
		public DateTime CalendarDay
		{
			get
			{
				return _CalendarDay;
			}
			set
			{
				_CalendarDay = value;
			}
		}

		#endregion

	}
}
