﻿
namespace Cre8NCutzApp.Forms
{
	partial class UserScrn
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lbl_WelcomeUser = new System.Windows.Forms.Label();
			this.pnl_UserScrn = new System.Windows.Forms.Panel();
			this.btn_UpdateNo = new System.Windows.Forms.Button();
			this.pnl_Scrn = new System.Windows.Forms.Panel();
			this.btn_Reports = new System.Windows.Forms.Button();
			this.btn_ExitUsrScrn = new System.Windows.Forms.Button();
			this.btn_Calendar = new System.Windows.Forms.Button();
			this.lbl_ChooseUserScrn = new System.Windows.Forms.Label();
			this.btn_CxInfo = new System.Windows.Forms.Button();
			this.btn_Appointments = new System.Windows.Forms.Button();
			this.picbox_UserScrn = new System.Windows.Forms.PictureBox();
			this.pnl_UserScrn.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.picbox_UserScrn)).BeginInit();
			this.SuspendLayout();
			// 
			// lbl_WelcomeUser
			// 
			this.lbl_WelcomeUser.BackColor = System.Drawing.Color.Transparent;
			this.lbl_WelcomeUser.Font = new System.Drawing.Font("Monotype Corsiva", 30F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbl_WelcomeUser.ForeColor = System.Drawing.Color.BlueViolet;
			this.lbl_WelcomeUser.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.lbl_WelcomeUser.Location = new System.Drawing.Point(8, 44);
			this.lbl_WelcomeUser.Margin = new System.Windows.Forms.Padding(5, 0, 0, 0);
			this.lbl_WelcomeUser.Name = "lbl_WelcomeUser";
			this.lbl_WelcomeUser.Size = new System.Drawing.Size(299, 43);
			this.lbl_WelcomeUser.TabIndex = 0;
			this.lbl_WelcomeUser.Text = "User Screen";
			// 
			// pnl_UserScrn
			// 
			this.pnl_UserScrn.BackColor = System.Drawing.Color.Indigo;
			this.pnl_UserScrn.BackgroundImage = global::Cre8NCutzApp.Properties.Resources.schedulerbackgrnd;
			this.pnl_UserScrn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
			this.pnl_UserScrn.Controls.Add(this.btn_UpdateNo);
			this.pnl_UserScrn.Controls.Add(this.pnl_Scrn);
			this.pnl_UserScrn.Controls.Add(this.btn_Reports);
			this.pnl_UserScrn.Controls.Add(this.btn_ExitUsrScrn);
			this.pnl_UserScrn.Controls.Add(this.btn_Calendar);
			this.pnl_UserScrn.Controls.Add(this.lbl_WelcomeUser);
			this.pnl_UserScrn.Controls.Add(this.lbl_ChooseUserScrn);
			this.pnl_UserScrn.Controls.Add(this.btn_CxInfo);
			this.pnl_UserScrn.Controls.Add(this.btn_Appointments);
			this.pnl_UserScrn.Controls.Add(this.picbox_UserScrn);
			this.pnl_UserScrn.Location = new System.Drawing.Point(0, 0);
			this.pnl_UserScrn.Name = "pnl_UserScrn";
			this.pnl_UserScrn.Size = new System.Drawing.Size(806, 522);
			this.pnl_UserScrn.TabIndex = 10;
			this.pnl_UserScrn.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnl_UserScrn_MouseDown);
			this.pnl_UserScrn.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pnl_UserScrn_MouseMove);
			this.pnl_UserScrn.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pnl_UserScrn_MouseUp);
			// 
			// btn_UpdateNo
			// 
			this.btn_UpdateNo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.btn_UpdateNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btn_UpdateNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_UpdateNo.ForeColor = System.Drawing.Color.SkyBlue;
			this.btn_UpdateNo.Location = new System.Drawing.Point(25, 457);
			this.btn_UpdateNo.Name = "btn_UpdateNo";
			this.btn_UpdateNo.Size = new System.Drawing.Size(140, 40);
			this.btn_UpdateNo.TabIndex = 19;
			this.btn_UpdateNo.Text = "Update # \r\nfor SMS";
			this.btn_UpdateNo.UseVisualStyleBackColor = false;
			this.btn_UpdateNo.Click += new System.EventHandler(this.btn_UpdateNo_Click);
			// 
			// pnl_Scrn
			// 
			this.pnl_Scrn.BackColor = System.Drawing.Color.Transparent;
			this.pnl_Scrn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.pnl_Scrn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
			this.pnl_Scrn.Location = new System.Drawing.Point(262, 127);
			this.pnl_Scrn.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
			this.pnl_Scrn.Name = "pnl_Scrn";
			this.pnl_Scrn.Size = new System.Drawing.Size(476, 306);
			this.pnl_Scrn.TabIndex = 17;
			this.pnl_Scrn.Visible = false;
			// 
			// btn_Reports
			// 
			this.btn_Reports.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.btn_Reports.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btn_Reports.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_Reports.ForeColor = System.Drawing.Color.SkyBlue;
			this.btn_Reports.Location = new System.Drawing.Point(25, 375);
			this.btn_Reports.Name = "btn_Reports";
			this.btn_Reports.Size = new System.Drawing.Size(140, 40);
			this.btn_Reports.TabIndex = 16;
			this.btn_Reports.Text = "Reports";
			this.btn_Reports.UseVisualStyleBackColor = false;
			this.btn_Reports.Click += new System.EventHandler(this.btn_Reports_Click);
			// 
			// btn_ExitUsrScrn
			// 
			this.btn_ExitUsrScrn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(63)))), ((int)(((byte)(150)))));
			this.btn_ExitUsrScrn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btn_ExitUsrScrn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
			this.btn_ExitUsrScrn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(63)))), ((int)(((byte)(150)))));
			this.btn_ExitUsrScrn.Location = new System.Drawing.Point(772, 5);
			this.btn_ExitUsrScrn.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.btn_ExitUsrScrn.Name = "btn_ExitUsrScrn";
			this.btn_ExitUsrScrn.Size = new System.Drawing.Size(24, 24);
			this.btn_ExitUsrScrn.TabIndex = 12;
			this.btn_ExitUsrScrn.Text = "X";
			this.btn_ExitUsrScrn.UseVisualStyleBackColor = true;
			this.btn_ExitUsrScrn.Click += new System.EventHandler(this.btn_ExitUsrScrn_Click);
			// 
			// btn_Calendar
			// 
			this.btn_Calendar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.btn_Calendar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btn_Calendar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_Calendar.ForeColor = System.Drawing.Color.SkyBlue;
			this.btn_Calendar.Location = new System.Drawing.Point(25, 293);
			this.btn_Calendar.Name = "btn_Calendar";
			this.btn_Calendar.Size = new System.Drawing.Size(140, 40);
			this.btn_Calendar.TabIndex = 9;
			this.btn_Calendar.Text = "View Calendar";
			this.btn_Calendar.UseVisualStyleBackColor = false;
			this.btn_Calendar.Click += new System.EventHandler(this.btn_Calendar_Click);
			// 
			// lbl_ChooseUserScrn
			// 
			this.lbl_ChooseUserScrn.BackColor = System.Drawing.Color.Transparent;
			this.lbl_ChooseUserScrn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(63)))), ((int)(((byte)(150)))));
			this.lbl_ChooseUserScrn.Location = new System.Drawing.Point(23, 94);
			this.lbl_ChooseUserScrn.Name = "lbl_ChooseUserScrn";
			this.lbl_ChooseUserScrn.Size = new System.Drawing.Size(284, 34);
			this.lbl_ChooseUserScrn.TabIndex = 1;
			this.lbl_ChooseUserScrn.Text = "Choose an option below";
			// 
			// btn_CxInfo
			// 
			this.btn_CxInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.btn_CxInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btn_CxInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_CxInfo.ForeColor = System.Drawing.Color.SkyBlue;
			this.btn_CxInfo.Location = new System.Drawing.Point(25, 129);
			this.btn_CxInfo.Name = "btn_CxInfo";
			this.btn_CxInfo.Size = new System.Drawing.Size(140, 40);
			this.btn_CxInfo.TabIndex = 7;
			this.btn_CxInfo.Text = "Customer";
			this.btn_CxInfo.UseVisualStyleBackColor = false;
			this.btn_CxInfo.Click += new System.EventHandler(this.btn_CxInfo_Click);
			// 
			// btn_Appointments
			// 
			this.btn_Appointments.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.btn_Appointments.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btn_Appointments.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_Appointments.ForeColor = System.Drawing.Color.SkyBlue;
			this.btn_Appointments.Location = new System.Drawing.Point(25, 211);
			this.btn_Appointments.Name = "btn_Appointments";
			this.btn_Appointments.Size = new System.Drawing.Size(140, 40);
			this.btn_Appointments.TabIndex = 8;
			this.btn_Appointments.Text = "Appointments";
			this.btn_Appointments.UseVisualStyleBackColor = false;
			this.btn_Appointments.Click += new System.EventHandler(this.btn_Appointments_Click);
			// 
			// picbox_UserScrn
			// 
			this.picbox_UserScrn.BackColor = System.Drawing.Color.Transparent;
			this.picbox_UserScrn.Location = new System.Drawing.Point(5, 12);
			this.picbox_UserScrn.Name = "picbox_UserScrn";
			this.picbox_UserScrn.Size = new System.Drawing.Size(796, 505);
			this.picbox_UserScrn.TabIndex = 18;
			this.picbox_UserScrn.TabStop = false;
			// 
			// UserScrn
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(14F, 26F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.ClientSize = new System.Drawing.Size(806, 522);
			this.Controls.Add(this.pnl_UserScrn);
			this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold);
			this.ForeColor = System.Drawing.Color.White;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
			this.Name = "UserScrn";
			this.Load += new System.EventHandler(this.UserScrn_Load);
			this.pnl_UserScrn.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.picbox_UserScrn)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Label lbl_WelcomeUser;
		private System.Windows.Forms.Panel pnl_UserScrn;
		private System.Windows.Forms.Button btn_ExitUsrScrn;
		private System.Windows.Forms.Panel pnl_Scrn;
		private System.Windows.Forms.Button btn_Reports;
		private System.Windows.Forms.Button btn_Calendar;
		private System.Windows.Forms.Label lbl_ChooseUserScrn;
		private System.Windows.Forms.Button btn_CxInfo;
		private System.Windows.Forms.Button btn_Appointments;
		private System.Windows.Forms.PictureBox picbox_UserScrn;
		private System.Windows.Forms.Button btn_UpdateNo;
	}
}