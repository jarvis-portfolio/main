﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Cre8NCutzApp.Forms;
using System.Runtime.InteropServices;
using Cre8NCutzApp.Controllers;
using Cre8NCutzApp.Models;
using System.Reflection;


namespace Cre8NCutzApp
{
	public partial class LoginScrn : Form
	{
		
		public static LoginScrn InstLoginScrn;
		
		public LoginScrn()
		{
			InitializeComponent();
			CreateInstances();
		}

		UserModel UserModel = new UserModel();
		
		private void CreateInstances()
		{
			InstLoginScrn = this;
		}

		private void btn_Login_Click(object sender, EventArgs e)
		{
			
			string user = txt_Username.Text;
			string pass = txt_Password.Text;
			bool login = DBControls.InstDBControls.IsValidLogin(user, pass);

			if (login)
			{
				UserScrn User = new UserScrn();
				User.ShowDialog();
			}
			else
			{
				MessageBox.Show(GlobalLogin.MsgBox_InvalidLoginText);
			}
		}
		private void btn_ExitLoginScrn_Click(object sender, EventArgs e)
		{
			LoginScrn.ActiveForm.Close();
		}

		int movX, movY;
		bool isMoving;
		private void pnl_Login_MouseDown(object sender, MouseEventArgs e)
		{
			isMoving = true;
			movX = e.X;
			movY = e.Y;
		}
		private void pnl_Login_MouseMove(object sender, MouseEventArgs e)
		{
			if (isMoving)
			{
				this.SetDesktopLocation(MousePosition.X - movX, MousePosition.Y - movY);
			}
		}

		private void pnl_Login_MouseUp(object sender, MouseEventArgs e)
		{
			isMoving = false;
		}

		private void LoginScrn_Load(object sender, EventArgs e)
		{
			//DBConnect.InstDBConnect.OpenUserDB();
			
		}

		private void LoginScrn_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter)
			{
				string user = txt_Username.Text;
				string pass = txt_Password.Text;
				bool login = DBControls.InstDBControls.IsValidLogin(user, pass);

				if (login)
				{
					UserScrn User = new UserScrn();
					User.ShowDialog();
				}
				else
				{
					MessageBox.Show(GlobalLogin.MsgBox_InvalidLoginText);
				}
			}
		}
		


	}
}
