﻿
namespace Cre8NCutzApp.Forms
{
	partial class NumberScrn
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.pnl_Reports = new System.Windows.Forms.Panel();
			this.lbl_UpdateNo = new System.Windows.Forms.Label();
			this.btn_UserNoUpdate = new System.Windows.Forms.Button();
			this.txt_UserPhone = new System.Windows.Forms.TextBox();
			this.lbl_UserPhone = new System.Windows.Forms.Label();
			this.pnl_Reports.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnl_Reports
			// 
			this.pnl_Reports.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.pnl_Reports.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.pnl_Reports.Controls.Add(this.txt_UserPhone);
			this.pnl_Reports.Controls.Add(this.lbl_UserPhone);
			this.pnl_Reports.Controls.Add(this.btn_UserNoUpdate);
			this.pnl_Reports.Controls.Add(this.lbl_UpdateNo);
			this.pnl_Reports.Location = new System.Drawing.Point(15, 15);
			this.pnl_Reports.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.pnl_Reports.Name = "pnl_Reports";
			this.pnl_Reports.Size = new System.Drawing.Size(684, 440);
			this.pnl_Reports.TabIndex = 24;
			// 
			// lbl_UpdateNo
			// 
			this.lbl_UpdateNo.AutoSize = true;
			this.lbl_UpdateNo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.lbl_UpdateNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbl_UpdateNo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(63)))), ((int)(((byte)(150)))));
			this.lbl_UpdateNo.Location = new System.Drawing.Point(30, 15);
			this.lbl_UpdateNo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lbl_UpdateNo.Name = "lbl_UpdateNo";
			this.lbl_UpdateNo.Size = new System.Drawing.Size(179, 26);
			this.lbl_UpdateNo.TabIndex = 1;
			this.lbl_UpdateNo.Text = "Update Number";
			// 
			// btn_UserNoUpdate
			// 
			this.btn_UserNoUpdate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.btn_UserNoUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btn_UserNoUpdate.ForeColor = System.Drawing.Color.SkyBlue;
			this.btn_UserNoUpdate.Location = new System.Drawing.Point(271, 158);
			this.btn_UserNoUpdate.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.btn_UserNoUpdate.Name = "btn_UserNoUpdate";
			this.btn_UserNoUpdate.Size = new System.Drawing.Size(112, 35);
			this.btn_UserNoUpdate.TabIndex = 117;
			this.btn_UserNoUpdate.Text = "Update";
			this.btn_UserNoUpdate.UseVisualStyleBackColor = false;
			this.btn_UserNoUpdate.Click += new System.EventHandler(this.btn_UserNoUpdate_Click);
			// 
			// txt_UserPhone
			// 
			this.txt_UserPhone.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.txt_UserPhone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txt_UserPhone.ForeColor = System.Drawing.Color.SkyBlue;
			this.txt_UserPhone.Location = new System.Drawing.Point(315, 97);
			this.txt_UserPhone.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.txt_UserPhone.Name = "txt_UserPhone";
			this.txt_UserPhone.Size = new System.Drawing.Size(149, 26);
			this.txt_UserPhone.TabIndex = 119;
			this.txt_UserPhone.Enter += new System.EventHandler(this.txt_UserPhone_Enter);
			this.txt_UserPhone.Leave += new System.EventHandler(this.txt_UserPhone_Leave);
			// 
			// lbl_UserPhone
			// 
			this.lbl_UserPhone.AutoSize = true;
			this.lbl_UserPhone.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.lbl_UserPhone.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(63)))), ((int)(((byte)(150)))));
			this.lbl_UserPhone.Location = new System.Drawing.Point(190, 103);
			this.lbl_UserPhone.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lbl_UserPhone.Name = "lbl_UserPhone";
			this.lbl_UserPhone.Size = new System.Drawing.Size(64, 20);
			this.lbl_UserPhone.TabIndex = 118;
			this.lbl_UserPhone.Text = "Phone#";
			// 
			// NumberScrn
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.ClientSize = new System.Drawing.Size(714, 471);
			this.Controls.Add(this.pnl_Reports);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "NumberScrn";
			this.Text = "NumberScrn";
			this.pnl_Reports.ResumeLayout(false);
			this.pnl_Reports.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel pnl_Reports;
		private System.Windows.Forms.Label lbl_UpdateNo;
		private System.Windows.Forms.Button btn_UserNoUpdate;
		public System.Windows.Forms.TextBox txt_UserPhone;
		private System.Windows.Forms.Label lbl_UserPhone;
	}
}