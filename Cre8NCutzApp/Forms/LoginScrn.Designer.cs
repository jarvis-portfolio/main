﻿
namespace Cre8NCutzApp
{
	partial class LoginScrn
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoginScrn));
			this.pnl_Login = new System.Windows.Forms.Panel();
			this.btn_ExitLoginScrn = new System.Windows.Forms.Button();
			this.picbox_UserIcon = new System.Windows.Forms.PictureBox();
			this.lbl_BuiltBy = new System.Windows.Forms.Label();
			this.btn_Login = new System.Windows.Forms.Button();
			this.txt_Password = new System.Windows.Forms.TextBox();
			this.txt_Username = new System.Windows.Forms.TextBox();
			this.lbl_PleaseLogin = new System.Windows.Forms.Label();
			this.lbl_SchedulingAppLogo = new System.Windows.Forms.Label();
			this.pnl_Login.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.picbox_UserIcon)).BeginInit();
			this.SuspendLayout();
			// 
			// pnl_Login
			// 
			resources.ApplyResources(this.pnl_Login, "pnl_Login");
			this.pnl_Login.BackColor = System.Drawing.Color.Indigo;
			this.pnl_Login.BackgroundImage = global::Cre8NCutzApp.Properties.Resources.loginbackgrnd;
			this.pnl_Login.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.pnl_Login.Controls.Add(this.btn_ExitLoginScrn);
			this.pnl_Login.Controls.Add(this.picbox_UserIcon);
			this.pnl_Login.Controls.Add(this.lbl_BuiltBy);
			this.pnl_Login.Controls.Add(this.btn_Login);
			this.pnl_Login.Controls.Add(this.txt_Password);
			this.pnl_Login.Controls.Add(this.txt_Username);
			this.pnl_Login.Controls.Add(this.lbl_PleaseLogin);
			this.pnl_Login.Controls.Add(this.lbl_SchedulingAppLogo);
			this.pnl_Login.Name = "pnl_Login";
			this.pnl_Login.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnl_Login_MouseDown);
			this.pnl_Login.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pnl_Login_MouseMove);
			this.pnl_Login.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pnl_Login_MouseUp);
			// 
			// btn_ExitLoginScrn
			// 
			resources.ApplyResources(this.btn_ExitLoginScrn, "btn_ExitLoginScrn");
			this.btn_ExitLoginScrn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(63)))), ((int)(((byte)(150)))));
			this.btn_ExitLoginScrn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(63)))), ((int)(((byte)(150)))));
			this.btn_ExitLoginScrn.Name = "btn_ExitLoginScrn";
			this.btn_ExitLoginScrn.UseVisualStyleBackColor = true;
			this.btn_ExitLoginScrn.Click += new System.EventHandler(this.btn_ExitLoginScrn_Click);
			// 
			// picbox_UserIcon
			// 
			resources.ApplyResources(this.picbox_UserIcon, "picbox_UserIcon");
			this.picbox_UserIcon.BackColor = System.Drawing.Color.BlueViolet;
			this.picbox_UserIcon.Name = "picbox_UserIcon";
			this.picbox_UserIcon.TabStop = false;
			// 
			// lbl_BuiltBy
			// 
			resources.ApplyResources(this.lbl_BuiltBy, "lbl_BuiltBy");
			this.lbl_BuiltBy.BackColor = System.Drawing.Color.Transparent;
			this.lbl_BuiltBy.ForeColor = System.Drawing.SystemColors.Control;
			this.lbl_BuiltBy.Name = "lbl_BuiltBy";
			// 
			// btn_Login
			// 
			resources.ApplyResources(this.btn_Login, "btn_Login");
			this.btn_Login.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.btn_Login.ForeColor = System.Drawing.Color.SkyBlue;
			this.btn_Login.Name = "btn_Login";
			this.btn_Login.UseVisualStyleBackColor = false;
			this.btn_Login.Click += new System.EventHandler(this.btn_Login_Click);
			// 
			// txt_Password
			// 
			resources.ApplyResources(this.txt_Password, "txt_Password");
			this.txt_Password.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.txt_Password.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.txt_Password.ForeColor = System.Drawing.Color.SkyBlue;
			this.txt_Password.Name = "txt_Password";
			this.txt_Password.UseSystemPasswordChar = true;
			// 
			// txt_Username
			// 
			resources.ApplyResources(this.txt_Username, "txt_Username");
			this.txt_Username.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.txt_Username.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.txt_Username.ForeColor = System.Drawing.Color.SkyBlue;
			this.txt_Username.Name = "txt_Username";
			// 
			// lbl_PleaseLogin
			// 
			resources.ApplyResources(this.lbl_PleaseLogin, "lbl_PleaseLogin");
			this.lbl_PleaseLogin.BackColor = System.Drawing.Color.Transparent;
			this.lbl_PleaseLogin.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(63)))), ((int)(((byte)(150)))));
			this.lbl_PleaseLogin.Name = "lbl_PleaseLogin";
			// 
			// lbl_SchedulingAppLogo
			// 
			resources.ApplyResources(this.lbl_SchedulingAppLogo, "lbl_SchedulingAppLogo");
			this.lbl_SchedulingAppLogo.BackColor = System.Drawing.Color.Transparent;
			this.lbl_SchedulingAppLogo.ForeColor = System.Drawing.Color.BlueViolet;
			this.lbl_SchedulingAppLogo.Name = "lbl_SchedulingAppLogo";
			// 
			// LoginScrn
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.DimGray;
			this.Controls.Add(this.pnl_Login);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.KeyPreview = true;
			this.Name = "LoginScrn";
			this.Load += new System.EventHandler(this.LoginScrn_Load);
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LoginScrn_KeyDown);
			this.pnl_Login.ResumeLayout(false);
			this.pnl_Login.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.picbox_UserIcon)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Label lbl_SchedulingAppLogo;
		private System.Windows.Forms.Panel pnl_Login;
		private System.Windows.Forms.Label lbl_BuiltBy;
		private System.Windows.Forms.Label lbl_PleaseLogin;
		private System.Windows.Forms.Button btn_Login;
		private System.Windows.Forms.PictureBox picbox_UserIcon;
		private System.Windows.Forms.Button btn_ExitLoginScrn;
		public System.Windows.Forms.TextBox txt_Username;
		public System.Windows.Forms.TextBox txt_Password;
	}
}

