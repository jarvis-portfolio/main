﻿
namespace Cre8NCutzApp.Forms
{
	partial class ReportScrn
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.pnl_Reports = new System.Windows.Forms.Panel();
			this.dd_ByType = new System.Windows.Forms.ComboBox();
			this.dd_ByMonth = new System.Windows.Forms.ComboBox();
			this.dd_ByUser = new System.Windows.Forms.ComboBox();
			this.dgv_Reports = new System.Windows.Forms.DataGridView();
			this.dd_ReportList = new System.Windows.Forms.ComboBox();
			this.lbl_Reports = new System.Windows.Forms.Label();
			this.pnl_Reports.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgv_Reports)).BeginInit();
			this.SuspendLayout();
			// 
			// pnl_Reports
			// 
			this.pnl_Reports.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.pnl_Reports.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.pnl_Reports.Controls.Add(this.dd_ByType);
			this.pnl_Reports.Controls.Add(this.dd_ByMonth);
			this.pnl_Reports.Controls.Add(this.dd_ByUser);
			this.pnl_Reports.Controls.Add(this.dgv_Reports);
			this.pnl_Reports.Controls.Add(this.dd_ReportList);
			this.pnl_Reports.Controls.Add(this.lbl_Reports);
			this.pnl_Reports.Location = new System.Drawing.Point(15, 15);
			this.pnl_Reports.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.pnl_Reports.Name = "pnl_Reports";
			this.pnl_Reports.Size = new System.Drawing.Size(684, 440);
			this.pnl_Reports.TabIndex = 23;
			// 
			// dd_ByType
			// 
			this.dd_ByType.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.dd_ByType.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.dd_ByType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.dd_ByType.ForeColor = System.Drawing.Color.SkyBlue;
			this.dd_ByType.FormattingEnabled = true;
			this.dd_ByType.Items.AddRange(new object[] {
            "Convention",
            "Deadline",
            "Retrospective",
            "Shipment-In",
            "Shipment-Out",
            "Pre-Order",
            "Market"});
			this.dd_ByType.Location = new System.Drawing.Point(348, 92);
			this.dd_ByType.Name = "dd_ByType";
			this.dd_ByType.Size = new System.Drawing.Size(148, 28);
			this.dd_ByType.TabIndex = 14;
			this.dd_ByType.Text = "By Type";
			this.dd_ByType.Visible = false;
			this.dd_ByType.SelectedIndexChanged += new System.EventHandler(this.dd_ByType_SelectedIndexChanged);
			// 
			// dd_ByMonth
			// 
			this.dd_ByMonth.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.dd_ByMonth.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.dd_ByMonth.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.dd_ByMonth.ForeColor = System.Drawing.Color.SkyBlue;
			this.dd_ByMonth.FormattingEnabled = true;
			this.dd_ByMonth.Items.AddRange(new object[] {
            "Jan",
            "Feb",
            "Mar",
            "Apr",
            "May",
            "June",
            "July",
            "Aug",
            "Sept",
            "Oct",
            "Nov",
            "Dec"});
			this.dd_ByMonth.Location = new System.Drawing.Point(183, 92);
			this.dd_ByMonth.Name = "dd_ByMonth";
			this.dd_ByMonth.Size = new System.Drawing.Size(121, 28);
			this.dd_ByMonth.TabIndex = 13;
			this.dd_ByMonth.Text = "By Month";
			this.dd_ByMonth.Visible = false;
			this.dd_ByMonth.SelectedIndexChanged += new System.EventHandler(this.dd_ByMonth_SelectedIndexChanged);
			// 
			// dd_ByUser
			// 
			this.dd_ByUser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.dd_ByUser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.dd_ByUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.dd_ByUser.ForeColor = System.Drawing.Color.SkyBlue;
			this.dd_ByUser.FormattingEnabled = true;
			this.dd_ByUser.Location = new System.Drawing.Point(280, 92);
			this.dd_ByUser.Name = "dd_ByUser";
			this.dd_ByUser.Size = new System.Drawing.Size(121, 28);
			this.dd_ByUser.TabIndex = 12;
			this.dd_ByUser.Text = "By User";
			this.dd_ByUser.Visible = false;
			this.dd_ByUser.SelectedIndexChanged += new System.EventHandler(this.dd_ByUser_SelectedIndexChanged);
			// 
			// dgv_Reports
			// 
			this.dgv_Reports.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.dgv_Reports.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.dgv_Reports.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgv_Reports.Location = new System.Drawing.Point(78, 132);
			this.dgv_Reports.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.dgv_Reports.Name = "dgv_Reports";
			this.dgv_Reports.RowHeadersWidth = 62;
			this.dgv_Reports.Size = new System.Drawing.Size(526, 284);
			this.dgv_Reports.TabIndex = 11;
			// 
			// dd_ReportList
			// 
			this.dd_ReportList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.dd_ReportList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.dd_ReportList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.dd_ReportList.ForeColor = System.Drawing.Color.SkyBlue;
			this.dd_ReportList.FormattingEnabled = true;
			this.dd_ReportList.Items.AddRange(new object[] {
            "Appointment Types - This Month",
            "Schedule for User",
            "Number of Customers"});
			this.dd_ReportList.Location = new System.Drawing.Point(183, 58);
			this.dd_ReportList.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.dd_ReportList.Name = "dd_ReportList";
			this.dd_ReportList.Size = new System.Drawing.Size(314, 28);
			this.dd_ReportList.TabIndex = 10;
			this.dd_ReportList.Text = "Report List";
			this.dd_ReportList.SelectedIndexChanged += new System.EventHandler(this.dd_ReportList_SelectedIndexChanged);
			// 
			// lbl_Reports
			// 
			this.lbl_Reports.AutoSize = true;
			this.lbl_Reports.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.lbl_Reports.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbl_Reports.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(63)))), ((int)(((byte)(150)))));
			this.lbl_Reports.Location = new System.Drawing.Point(30, 15);
			this.lbl_Reports.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lbl_Reports.Name = "lbl_Reports";
			this.lbl_Reports.Size = new System.Drawing.Size(95, 26);
			this.lbl_Reports.TabIndex = 1;
			this.lbl_Reports.Text = "Reports";
			// 
			// ReportScrn
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.ClientSize = new System.Drawing.Size(714, 471);
			this.Controls.Add(this.pnl_Reports);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.Name = "ReportScrn";
			this.Text = "ReportScrn";
			this.pnl_Reports.ResumeLayout(false);
			this.pnl_Reports.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgv_Reports)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel pnl_Reports;
		private System.Windows.Forms.DataGridView dgv_Reports;
		private System.Windows.Forms.ComboBox dd_ReportList;
		private System.Windows.Forms.Label lbl_Reports;
		private System.Windows.Forms.ComboBox dd_ByUser;
		private System.Windows.Forms.ComboBox dd_ByType;
		private System.Windows.Forms.ComboBox dd_ByMonth;
	}
}