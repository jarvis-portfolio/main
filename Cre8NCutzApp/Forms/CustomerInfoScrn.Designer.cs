﻿
namespace Cre8NCutzApp.Forms
{
	partial class CustomerInfoScrn
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.pnl_CxInfo = new System.Windows.Forms.Panel();
			this.btn_CxClear = new System.Windows.Forms.Button();
			this.txt_CxCountry = new System.Windows.Forms.TextBox();
			this.txt_CxCity = new System.Windows.Forms.TextBox();
			this.txt_CxPhone = new System.Windows.Forms.TextBox();
			this.txt_CxName = new System.Windows.Forms.TextBox();
			this.txt_CxAddress = new System.Windows.Forms.TextBox();
			this.txt_CxID = new System.Windows.Forms.TextBox();
			this.btn_CxDelete = new System.Windows.Forms.Button();
			this.btn_CxNew = new System.Windows.Forms.Button();
			this.btn_CxUpdate = new System.Windows.Forms.Button();
			this.lbl_CxCountry = new System.Windows.Forms.Label();
			this.lbl_CxCity = new System.Windows.Forms.Label();
			this.lbl_CxPhone = new System.Windows.Forms.Label();
			this.lbl_CxAddress = new System.Windows.Forms.Label();
			this.lbl_CxID = new System.Windows.Forms.Label();
			this.lbl_CxName = new System.Windows.Forms.Label();
			this.dd_CustomerList = new System.Windows.Forms.ComboBox();
			this.lbl_CustomerInfo = new System.Windows.Forms.Label();
			this.pnl_CxInfo.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnl_CxInfo
			// 
			this.pnl_CxInfo.BackColor = System.Drawing.Color.Transparent;
			this.pnl_CxInfo.Controls.Add(this.btn_CxClear);
			this.pnl_CxInfo.Controls.Add(this.txt_CxCountry);
			this.pnl_CxInfo.Controls.Add(this.txt_CxCity);
			this.pnl_CxInfo.Controls.Add(this.txt_CxPhone);
			this.pnl_CxInfo.Controls.Add(this.txt_CxName);
			this.pnl_CxInfo.Controls.Add(this.txt_CxAddress);
			this.pnl_CxInfo.Controls.Add(this.txt_CxID);
			this.pnl_CxInfo.Controls.Add(this.btn_CxDelete);
			this.pnl_CxInfo.Controls.Add(this.btn_CxNew);
			this.pnl_CxInfo.Controls.Add(this.btn_CxUpdate);
			this.pnl_CxInfo.Controls.Add(this.lbl_CxCountry);
			this.pnl_CxInfo.Controls.Add(this.lbl_CxCity);
			this.pnl_CxInfo.Controls.Add(this.lbl_CxPhone);
			this.pnl_CxInfo.Controls.Add(this.lbl_CxAddress);
			this.pnl_CxInfo.Controls.Add(this.lbl_CxID);
			this.pnl_CxInfo.Controls.Add(this.lbl_CxName);
			this.pnl_CxInfo.Controls.Add(this.dd_CustomerList);
			this.pnl_CxInfo.Controls.Add(this.lbl_CustomerInfo);
			this.pnl_CxInfo.Location = new System.Drawing.Point(15, 15);
			this.pnl_CxInfo.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.pnl_CxInfo.Name = "pnl_CxInfo";
			this.pnl_CxInfo.Size = new System.Drawing.Size(684, 440);
			this.pnl_CxInfo.TabIndex = 0;
			// 
			// btn_CxClear
			// 
			this.btn_CxClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.btn_CxClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btn_CxClear.ForeColor = System.Drawing.Color.SkyBlue;
			this.btn_CxClear.Location = new System.Drawing.Point(52, 374);
			this.btn_CxClear.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.btn_CxClear.Name = "btn_CxClear";
			this.btn_CxClear.Size = new System.Drawing.Size(112, 35);
			this.btn_CxClear.TabIndex = 118;
			this.btn_CxClear.Text = "Clear";
			this.btn_CxClear.UseVisualStyleBackColor = false;
			this.btn_CxClear.Click += new System.EventHandler(this.btn_CxClear_Click);
			// 
			// txt_CxCountry
			// 
			this.txt_CxCountry.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.txt_CxCountry.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txt_CxCountry.ForeColor = System.Drawing.Color.SkyBlue;
			this.txt_CxCountry.Location = new System.Drawing.Point(324, 311);
			this.txt_CxCountry.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.txt_CxCountry.Name = "txt_CxCountry";
			this.txt_CxCountry.Size = new System.Drawing.Size(149, 26);
			this.txt_CxCountry.TabIndex = 114;
			this.txt_CxCountry.Enter += new System.EventHandler(this.txt_CxCountry_Enter);
			this.txt_CxCountry.Leave += new System.EventHandler(this.txt_CxCountry_Leave);
			// 
			// txt_CxCity
			// 
			this.txt_CxCity.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.txt_CxCity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txt_CxCity.ForeColor = System.Drawing.Color.SkyBlue;
			this.txt_CxCity.Location = new System.Drawing.Point(324, 272);
			this.txt_CxCity.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.txt_CxCity.Name = "txt_CxCity";
			this.txt_CxCity.Size = new System.Drawing.Size(149, 26);
			this.txt_CxCity.TabIndex = 113;
			this.txt_CxCity.Enter += new System.EventHandler(this.txt_CxCity_Enter);
			this.txt_CxCity.Leave += new System.EventHandler(this.txt_CxCity_Leave);
			// 
			// txt_CxPhone
			// 
			this.txt_CxPhone.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.txt_CxPhone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txt_CxPhone.ForeColor = System.Drawing.Color.SkyBlue;
			this.txt_CxPhone.Location = new System.Drawing.Point(324, 234);
			this.txt_CxPhone.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.txt_CxPhone.Name = "txt_CxPhone";
			this.txt_CxPhone.Size = new System.Drawing.Size(149, 26);
			this.txt_CxPhone.TabIndex = 112;
			this.txt_CxPhone.Enter += new System.EventHandler(this.txt_CxPhone_Enter);
			this.txt_CxPhone.Leave += new System.EventHandler(this.txt_CxPhone_Leave);
			// 
			// txt_CxName
			// 
			this.txt_CxName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.txt_CxName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txt_CxName.ForeColor = System.Drawing.Color.SkyBlue;
			this.txt_CxName.Location = new System.Drawing.Point(324, 157);
			this.txt_CxName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.txt_CxName.Name = "txt_CxName";
			this.txt_CxName.Size = new System.Drawing.Size(149, 26);
			this.txt_CxName.TabIndex = 110;
			this.txt_CxName.Enter += new System.EventHandler(this.txt_CxName_Enter);
			this.txt_CxName.Leave += new System.EventHandler(this.txt_CxName_Leave);
			// 
			// txt_CxAddress
			// 
			this.txt_CxAddress.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.txt_CxAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txt_CxAddress.ForeColor = System.Drawing.Color.SkyBlue;
			this.txt_CxAddress.Location = new System.Drawing.Point(324, 195);
			this.txt_CxAddress.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.txt_CxAddress.Name = "txt_CxAddress";
			this.txt_CxAddress.Size = new System.Drawing.Size(149, 26);
			this.txt_CxAddress.TabIndex = 111;
			this.txt_CxAddress.Enter += new System.EventHandler(this.txt_CxAddress_Enter);
			this.txt_CxAddress.Leave += new System.EventHandler(this.txt_CxAddress_Leave);
			// 
			// txt_CxID
			// 
			this.txt_CxID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.txt_CxID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txt_CxID.Enabled = false;
			this.txt_CxID.ForeColor = System.Drawing.Color.SkyBlue;
			this.txt_CxID.Location = new System.Drawing.Point(324, 118);
			this.txt_CxID.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.txt_CxID.Name = "txt_CxID";
			this.txt_CxID.Size = new System.Drawing.Size(149, 26);
			this.txt_CxID.TabIndex = 109;
			// 
			// btn_CxDelete
			// 
			this.btn_CxDelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.btn_CxDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btn_CxDelete.ForeColor = System.Drawing.Color.SkyBlue;
			this.btn_CxDelete.Location = new System.Drawing.Point(517, 374);
			this.btn_CxDelete.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.btn_CxDelete.Name = "btn_CxDelete";
			this.btn_CxDelete.Size = new System.Drawing.Size(112, 35);
			this.btn_CxDelete.TabIndex = 117;
			this.btn_CxDelete.Text = "Delete";
			this.btn_CxDelete.UseVisualStyleBackColor = false;
			this.btn_CxDelete.Click += new System.EventHandler(this.btn_CxDelete_Click);
			// 
			// btn_CxNew
			// 
			this.btn_CxNew.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.btn_CxNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btn_CxNew.ForeColor = System.Drawing.Color.SkyBlue;
			this.btn_CxNew.Location = new System.Drawing.Point(208, 374);
			this.btn_CxNew.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.btn_CxNew.Name = "btn_CxNew";
			this.btn_CxNew.Size = new System.Drawing.Size(112, 35);
			this.btn_CxNew.TabIndex = 115;
			this.btn_CxNew.Text = "Add New";
			this.btn_CxNew.UseVisualStyleBackColor = false;
			this.btn_CxNew.Click += new System.EventHandler(this.btn_CxNew_Click);
			// 
			// btn_CxUpdate
			// 
			this.btn_CxUpdate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.btn_CxUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btn_CxUpdate.ForeColor = System.Drawing.Color.SkyBlue;
			this.btn_CxUpdate.Location = new System.Drawing.Point(363, 374);
			this.btn_CxUpdate.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.btn_CxUpdate.Name = "btn_CxUpdate";
			this.btn_CxUpdate.Size = new System.Drawing.Size(112, 35);
			this.btn_CxUpdate.TabIndex = 116;
			this.btn_CxUpdate.Text = "Update";
			this.btn_CxUpdate.UseVisualStyleBackColor = false;
			this.btn_CxUpdate.Click += new System.EventHandler(this.btn_CxUpdate_Click);
			// 
			// lbl_CxCountry
			// 
			this.lbl_CxCountry.AutoSize = true;
			this.lbl_CxCountry.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.lbl_CxCountry.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(63)))), ((int)(((byte)(150)))));
			this.lbl_CxCountry.Location = new System.Drawing.Point(219, 314);
			this.lbl_CxCountry.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lbl_CxCountry.Name = "lbl_CxCountry";
			this.lbl_CxCountry.Size = new System.Drawing.Size(64, 20);
			this.lbl_CxCountry.TabIndex = 108;
			this.lbl_CxCountry.Text = "Country";
			// 
			// lbl_CxCity
			// 
			this.lbl_CxCity.AutoSize = true;
			this.lbl_CxCity.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.lbl_CxCity.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(63)))), ((int)(((byte)(150)))));
			this.lbl_CxCity.Location = new System.Drawing.Point(246, 275);
			this.lbl_CxCity.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lbl_CxCity.Name = "lbl_CxCity";
			this.lbl_CxCity.Size = new System.Drawing.Size(35, 20);
			this.lbl_CxCity.TabIndex = 106;
			this.lbl_CxCity.Text = "City";
			// 
			// lbl_CxPhone
			// 
			this.lbl_CxPhone.AutoSize = true;
			this.lbl_CxPhone.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.lbl_CxPhone.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(63)))), ((int)(((byte)(150)))));
			this.lbl_CxPhone.Location = new System.Drawing.Point(214, 237);
			this.lbl_CxPhone.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lbl_CxPhone.Name = "lbl_CxPhone";
			this.lbl_CxPhone.Size = new System.Drawing.Size(64, 20);
			this.lbl_CxPhone.TabIndex = 105;
			this.lbl_CxPhone.Text = "Phone#";
			// 
			// lbl_CxAddress
			// 
			this.lbl_CxAddress.AutoSize = true;
			this.lbl_CxAddress.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.lbl_CxAddress.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(63)))), ((int)(((byte)(150)))));
			this.lbl_CxAddress.Location = new System.Drawing.Point(214, 198);
			this.lbl_CxAddress.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lbl_CxAddress.Name = "lbl_CxAddress";
			this.lbl_CxAddress.Size = new System.Drawing.Size(68, 20);
			this.lbl_CxAddress.TabIndex = 104;
			this.lbl_CxAddress.Text = "Address";
			// 
			// lbl_CxID
			// 
			this.lbl_CxID.AutoSize = true;
			this.lbl_CxID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.lbl_CxID.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(63)))), ((int)(((byte)(150)))));
			this.lbl_CxID.Location = new System.Drawing.Point(255, 122);
			this.lbl_CxID.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lbl_CxID.Name = "lbl_CxID";
			this.lbl_CxID.Size = new System.Drawing.Size(26, 20);
			this.lbl_CxID.TabIndex = 103;
			this.lbl_CxID.Text = "ID";
			// 
			// lbl_CxName
			// 
			this.lbl_CxName.AutoSize = true;
			this.lbl_CxName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.lbl_CxName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(63)))), ((int)(((byte)(150)))));
			this.lbl_CxName.Location = new System.Drawing.Point(230, 160);
			this.lbl_CxName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lbl_CxName.Name = "lbl_CxName";
			this.lbl_CxName.Size = new System.Drawing.Size(51, 20);
			this.lbl_CxName.TabIndex = 102;
			this.lbl_CxName.Text = "Name";
			// 
			// dd_CustomerList
			// 
			this.dd_CustomerList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.dd_CustomerList.DisplayMember = "CustName";
			this.dd_CustomerList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.dd_CustomerList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.dd_CustomerList.ForeColor = System.Drawing.Color.SkyBlue;
			this.dd_CustomerList.FormattingEnabled = true;
			this.dd_CustomerList.Location = new System.Drawing.Point(255, 55);
			this.dd_CustomerList.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.dd_CustomerList.Name = "dd_CustomerList";
			this.dd_CustomerList.Size = new System.Drawing.Size(180, 28);
			this.dd_CustomerList.TabIndex = 107;
			this.dd_CustomerList.Text = "Choose Customer";
			this.dd_CustomerList.SelectedIndexChanged += new System.EventHandler(this.dd_CustomerList_SelectedIndexChanged);
			this.dd_CustomerList.Enter += new System.EventHandler(this.dd_CustomerList_Enter);
			this.dd_CustomerList.Leave += new System.EventHandler(this.dd_CustomerList_Leave);
			this.dd_CustomerList.MouseHover += new System.EventHandler(this.dd_CustomerList_MouseHover);
			// 
			// lbl_CustomerInfo
			// 
			this.lbl_CustomerInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.lbl_CustomerInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbl_CustomerInfo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(63)))), ((int)(((byte)(150)))));
			this.lbl_CustomerInfo.Location = new System.Drawing.Point(30, 15);
			this.lbl_CustomerInfo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lbl_CustomerInfo.Name = "lbl_CustomerInfo";
			this.lbl_CustomerInfo.Size = new System.Drawing.Size(172, 28);
			this.lbl_CustomerInfo.TabIndex = 101;
			this.lbl_CustomerInfo.Text = "Customer Info";
			// 
			// CustomerInfoScrn
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.ClientSize = new System.Drawing.Size(714, 471);
			this.Controls.Add(this.pnl_CxInfo);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.Name = "CustomerInfoScrn";
			this.Text = "CustomerInfoScrn";
			this.Load += new System.EventHandler(this.CustomerInfoScrn_Load);
			this.pnl_CxInfo.ResumeLayout(false);
			this.pnl_CxInfo.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel pnl_CxInfo;
		public System.Windows.Forms.TextBox txt_CxCountry;
		public System.Windows.Forms.TextBox txt_CxCity;
		public System.Windows.Forms.TextBox txt_CxPhone;
		public System.Windows.Forms.TextBox txt_CxName;
		public System.Windows.Forms.TextBox txt_CxAddress;
		public System.Windows.Forms.TextBox txt_CxID;
		private System.Windows.Forms.Button btn_CxDelete;
		private System.Windows.Forms.Button btn_CxNew;
		private System.Windows.Forms.Button btn_CxUpdate;
		private System.Windows.Forms.Label lbl_CxCountry;
		private System.Windows.Forms.Label lbl_CxCity;
		private System.Windows.Forms.Label lbl_CxPhone;
		private System.Windows.Forms.Label lbl_CxAddress;
		private System.Windows.Forms.Label lbl_CxID;
		private System.Windows.Forms.Label lbl_CxName;
		public System.Windows.Forms.ComboBox dd_CustomerList;
		private System.Windows.Forms.Label lbl_CustomerInfo;
		private System.Windows.Forms.Button btn_CxClear;
	}
}