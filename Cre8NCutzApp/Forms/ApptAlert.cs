﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cre8NCutzApp.Forms
{
	public partial class ApptAlert : Form
	{
		int movX, movY;
		bool isMoving;
		public static string startTime;

		public ApptAlert()
		{
			InitializeComponent();
			lbl_ApptAlert.Text += startTime;
			
		}

		private void btn_ApptClear_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void pnl_ApptAlert_MouseDown(object sender, MouseEventArgs e)
		{
			isMoving = true;
			movX = e.X;
			movY = e.Y;
		}

		private void pnl_ApptAlert_MouseMove(object sender, MouseEventArgs e)
		{
			if (isMoving)
			{
				this.SetDesktopLocation(MousePosition.X - movX, MousePosition.Y - movY);
			}
		}
		
		private void pnl_ApptAlert_MouseUp(object sender, MouseEventArgs e)
		{
			isMoving = false;
			
		}
		public static void AppointmentTime(string apptStart)
		{
			startTime = apptStart;
			ApptAlert Alert = new ApptAlert();
			Alert.ShowDialog();


		}
	}
		
}
