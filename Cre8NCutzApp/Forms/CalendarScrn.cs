﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Cre8NCutzApp.Controllers;

namespace Cre8NCutzApp.Forms
{
	//  - View Calender by week should take all the appointments and display each record's
	// Name, Appointment Type, and DateTime, Created By. Names can be color cooridanated to see the
	// difference in each record showing on the data grid. For each, different appointment id
	// show the data in random different hex color. Random for number between 1-255, for RGB.
	// Display the top row only shows week start of that sunday's date
	// .Text = "Week " + weekNumber + " " + monthName + " " + monthNumbersuffixed"; to show: Week 1 January 2nd
	// Middle row shows Name, DateTime in random color on the grid in bold
	// MessageBox when cell selected show more detail when cell has selected and focus Name, Appointment Type, DataTime Created by
	// "The FLS Appraisal Development appointment for Peter Parker created by Test, is at January 2nd at 3pm Central.
	// Would you like to update? Yes No"
	// Maybe with update have an option to turn on text to speech.
	// Month should show all appointments Name and time sorted top to bottom
	// Month grid showing with Name and Time sorted top to bottom cell in the cell matching Month and Day
	// Top show January 2022 (These fields may be better populated in a text box where I can control the view
	// Cells showing should be height adaptable by row depending on how many appointment showing
	// Remove ALL from the drop down. We don't need another calendar view
	// Enable _DoubleClick event to show the Appointment screen with Customer Selected
	public partial class CalendarScrn : Form
	{
		DataTable wkGrid = new DataTable();
		DataTable mthGrid = new DataTable();
		DateTime currentDate = DateTime.Now;
		int count;
		public CalendarScrn()
		{
			InitializeComponent();
		}

		private void dd_CalendarView_SelectedIndexChanged(object sender, EventArgs e)
		{
			if ("Weekly".Equals(dd_CalendarView.SelectedItem))
			{
				dgv_Calendar.DataSource = null;
				format_WeeklyCalView(cal_CalView);
				
			}
			else if ("Monthly".Equals(dd_CalendarView.SelectedItem))
			{
				dgv_Calendar.DataSource = null;
				format_MonthlyCalView(cal_CalView);
				
			}			
		}

		private void cal_CalView_DateSelected(object sender, DateRangeEventArgs e)
		{
			currentDate = e.Start;
			if ("Weekly".Equals(dd_CalendarView.SelectedItem))
			{
				format_WeeklyCalView(cal_CalView);
			}
			else if ("Monthly".Equals(dd_CalendarView.SelectedItem))
			{
				format_MonthlyCalView(cal_CalView);
			}
		}
		private void format_WeeklyCalView(MonthCalendar cal)
		{
			cal_CalView.RemoveAllBoldedDates();
			int currentDay = (int)currentDate.DayOfWeek;
			string beginDate = currentDate.AddDays(-currentDay).ToString("yyyy/MM/dd HH:mm:ss");
			string endDate = currentDate.AddDays(7 - currentDay).ToString("yyyy/MM/dd HH:mm:ss");
			DateTime tempDate = Convert.ToDateTime(beginDate);
			DateTime endTempDate = Convert.ToDateTime(endDate);
			for (int i = 0; i < 7; i++)
			{
				cal_CalView.AddBoldedDate(tempDate.AddDays(i));
			}
			cal_CalView.UpdateBoldedDates();
			wkGrid.Rows.Clear();
			DBControls.WeeklyCalendar(wkGrid, beginDate, endDate);
			if (wkGrid.Rows.Count > 0)
			{
				dgv_Calendar.DataSource = wkGrid;
				formatDGV_Calendar(dgv_Calendar);
			}

		}
		private void format_MonthlyCalView(MonthCalendar cal)
		{
			cal_CalView.RemoveAllBoldedDates();
			int currentDay = 0;
			int currentMonth = (int)currentDate.Month;
			
			DateTime beginCalDate = new DateTime(currentDate.Year, currentDate.Month, 1);
			DateTime endCalDate = beginCalDate.AddMonths(1).AddDays(-1);
			string beginDate = beginCalDate.ToString("yyyy/MM/dd HH:mm:ss"); 
			string endDate = endCalDate.ToString("yyyy/MM/dd HH:mm:ss"); 
			switch (currentMonth)
			{
				case 1:
				case 3:
				case 5:
				case 7:
				case 8:
				case 10:
					currentDay = 31;
					break;
				case 4:
				case 6:
				case 9:
				case 11:
					currentDay = 30;
					break;
				default:
					currentDay = 29;
					break;
			}
			for (count = 0; count < currentDay; count++)
			{
				cal_CalView.AddBoldedDate(beginCalDate.AddDays(count));
			}
			cal_CalView.UpdateBoldedDates();
			mthGrid.Rows.Clear();
			DBControls.MonthlyCalendar(mthGrid, beginDate, endDate);
			if (mthGrid.Rows.Count > 0)
			{
				dgv_Calendar.DataSource = mthGrid;
				formatDGV_Calendar(dgv_Calendar);
			}
			
		}
		private void formatDGV_Calendar(DataGridView e)
		{
			//  - Alternate colors on the grid to differentiate appointments. Set column headers.
			//  - Click events to open appointment screen to the appointment?

			e.DefaultCellStyle.SelectionBackColor = Color.SkyBlue;
			e.DefaultCellStyle.SelectionForeColor = Color.FromArgb(64, 64, 64);
			e.DefaultCellStyle.BackColor = Color.FromArgb(64, 64, 64);
			e.DefaultCellStyle.ForeColor = Color.SkyBlue;
			e.RowHeadersVisible = false;
			e.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);

			e.Columns[0].Visible = false;
			e.Columns[1].HeaderText = "Customer";
			e.Columns[2].Visible = false;
			e.Columns[3].Visible = false;
			e.Columns[4].HeaderText = "Type";
			e.Columns[5].HeaderText = "Start";
			
			dgv_Calendar.Font = new Font("Microsoft Sans Serif", 8);

			e.ReadOnly = true;

		}
		
	}
}
