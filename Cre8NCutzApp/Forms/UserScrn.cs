﻿using Cre8NCutzApp.Controllers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cre8NCutzApp.Forms
{
	public partial class UserScrn : Form
	{
		public UserScrn()
		{
			InitializeComponent();
			
		}

		int movX, movY;
		bool isMoving;
		private void btn_ExitUsrScrn_Click(object sender, EventArgs e)
		{
			UserScrn.ActiveForm.Close();
		}
		private void pnl_UserScrn_MouseDown(object sender, MouseEventArgs e)
		{
			isMoving = true;
			movX = e.X;
			movY = e.Y;
		}
		private void pnl_UserScrn_MouseMove(object sender, MouseEventArgs e)
		{
			if (isMoving)
			{
				this.SetDesktopLocation(MousePosition.X - movX, MousePosition.Y - movY);
			}
		}

		private void UserScrn_Load(object sender, EventArgs e)
		{
		}

		public void LoadScrn(object Form)
		{
			if (pnl_Scrn.Controls.Count > 0)
			{
				pnl_Scrn.Controls.RemoveAt(0);
			}
			Form scrn = Form as Form;
			scrn.TopLevel = false;
			
			this.pnl_Scrn.Controls.Add(scrn);
			scrn.Size = new Size(476, 306);
			pnl_Scrn.Visible = true;
			scrn.Show();
		}
		private void btn_CxInfo_Click(object sender, EventArgs e)
		{

			LoadScrn(new CustomerInfoScrn());
		}

		private void btn_Appointments_Click(object sender, EventArgs e)
		{
			LoadScrn(new AppointmentScrn());
		}

		private void btn_Calendar_Click(object sender, EventArgs e)
		{
			LoadScrn(new CalendarScrn());
		}

		private void calendar1_Load(object sender, EventArgs e)
		{

		}

		private void btn_Reports_Click(object sender, EventArgs e)
		{
			LoadScrn(new ReportScrn());
		}

		private void btn_UpdateNo_Click(object sender, EventArgs e)
		{
			LoadScrn(new NumberScrn());
		}

		private void pnl_UserScrn_MouseUp(object sender, MouseEventArgs e)
		{
			isMoving = false;
		}


	}
}
