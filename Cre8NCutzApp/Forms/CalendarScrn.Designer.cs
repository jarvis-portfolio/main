﻿
namespace Cre8NCutzApp.Forms
{
	partial class CalendarScrn
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.dgv_Calendar = new System.Windows.Forms.DataGridView();
			this.dd_CalendarView = new System.Windows.Forms.ComboBox();
			this.lbl_Calendar = new System.Windows.Forms.Label();
			this.pnl_Calendar = new System.Windows.Forms.Panel();
			this.cal_CalView = new System.Windows.Forms.MonthCalendar();
			((System.ComponentModel.ISupportInitialize)(this.dgv_Calendar)).BeginInit();
			this.pnl_Calendar.SuspendLayout();
			this.SuspendLayout();
			// 
			// dgv_Calendar
			// 
			this.dgv_Calendar.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.dgv_Calendar.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgv_Calendar.Location = new System.Drawing.Point(340, 134);
			this.dgv_Calendar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.dgv_Calendar.Name = "dgv_Calendar";
			this.dgv_Calendar.RowHeadersWidth = 62;
			this.dgv_Calendar.Size = new System.Drawing.Size(346, 249);
			this.dgv_Calendar.TabIndex = 11;
			// 
			// dd_CalendarView
			// 
			this.dd_CalendarView.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.dd_CalendarView.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.dd_CalendarView.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.dd_CalendarView.ForeColor = System.Drawing.Color.SkyBlue;
			this.dd_CalendarView.FormattingEnabled = true;
			this.dd_CalendarView.Items.AddRange(new object[] {
            "Weekly",
            "Monthly"});
			this.dd_CalendarView.Location = new System.Drawing.Point(255, 55);
			this.dd_CalendarView.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.dd_CalendarView.Name = "dd_CalendarView";
			this.dd_CalendarView.Size = new System.Drawing.Size(180, 28);
			this.dd_CalendarView.TabIndex = 10;
			this.dd_CalendarView.Text = "View Calendar By";
			this.dd_CalendarView.SelectedIndexChanged += new System.EventHandler(this.dd_CalendarView_SelectedIndexChanged);
			// 
			// lbl_Calendar
			// 
			this.lbl_Calendar.AutoSize = true;
			this.lbl_Calendar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.lbl_Calendar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbl_Calendar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(63)))), ((int)(((byte)(150)))));
			this.lbl_Calendar.Location = new System.Drawing.Point(30, 15);
			this.lbl_Calendar.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lbl_Calendar.Name = "lbl_Calendar";
			this.lbl_Calendar.Size = new System.Drawing.Size(108, 26);
			this.lbl_Calendar.TabIndex = 1;
			this.lbl_Calendar.Text = "Calendar";
			// 
			// pnl_Calendar
			// 
			this.pnl_Calendar.Controls.Add(this.cal_CalView);
			this.pnl_Calendar.Controls.Add(this.dgv_Calendar);
			this.pnl_Calendar.Controls.Add(this.dd_CalendarView);
			this.pnl_Calendar.Controls.Add(this.lbl_Calendar);
			this.pnl_Calendar.Location = new System.Drawing.Point(15, 15);
			this.pnl_Calendar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.pnl_Calendar.Name = "pnl_Calendar";
			this.pnl_Calendar.Size = new System.Drawing.Size(684, 440);
			this.pnl_Calendar.TabIndex = 12;
			// 
			// cal_CalView
			// 
			this.cal_CalView.BackColor = System.Drawing.Color.Gray;
			this.cal_CalView.Location = new System.Drawing.Point(-1, 134);
			this.cal_CalView.Name = "cal_CalView";
			this.cal_CalView.ShowToday = false;
			this.cal_CalView.TabIndex = 12;
			this.cal_CalView.DateSelected += new System.Windows.Forms.DateRangeEventHandler(this.cal_CalView_DateSelected);
			// 
			// CalendarScrn
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.ClientSize = new System.Drawing.Size(714, 471);
			this.Controls.Add(this.pnl_Calendar);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.Name = "CalendarScrn";
			this.Text = "CalendarScrn";
			((System.ComponentModel.ISupportInitialize)(this.dgv_Calendar)).EndInit();
			this.pnl_Calendar.ResumeLayout(false);
			this.pnl_Calendar.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion
		private System.Windows.Forms.DataGridView dgv_Calendar;
		private System.Windows.Forms.ComboBox dd_CalendarView;
		private System.Windows.Forms.Label lbl_Calendar;
		private System.Windows.Forms.Panel pnl_Calendar;
		private System.Windows.Forms.MonthCalendar cal_CalView;
	}
}