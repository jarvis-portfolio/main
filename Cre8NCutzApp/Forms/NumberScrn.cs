﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using Cre8NCutzApp.Controllers;

namespace Cre8NCutzApp.Forms
{
	public partial class NumberScrn : Form
	{
		DataTable userNoGrid = new DataTable();
		bool notHighlighted = true;

		public NumberScrn()
		{
			InitializeComponent();
			//userNoGrid.Rows.Clear();
			formattxt_UserNoView();
		}

		private void formattxt_UserNoView()
		{

			string thisUserNo;// = Int32.Parse(txt_UserPhone.Text);
			thisUserNo = DBControls.UserNoGrid();
			if (thisUserNo != "")
			{
				txt_UserPhone.Text = thisUserNo;
				//formatGridDGV_UserNo(dgv_UserNo);
			}
		}

		//private void formatGridDGV_UserNo(DataGridView e)
		//{
		//	e.DefaultCellStyle.BackColor = Color.FromArgb(64, 64, 64);
		//	e.DefaultCellStyle.ForeColor = Color.SkyBlue;
		//	e.RowHeadersVisible = false;
		//	e.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);

		//	e.Columns[0].Visible = true;
		//	e.Columns[1].Visible = true;
		//	e.Columns[0].HeaderText = "User";
		//	e.Columns[1].HeaderText = "Number";
		//	dgv_UserNo.Font = new Font("Microsoft Sans Serif", 8);

		//	//e.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
		//	e.ReadOnly = false;
		//}

		private void btn_UserNoUpdate_Click(object sender, EventArgs e)
		{

			if (notHighlighted)
			{

				DialogResult actionUpdateCx = MessageBox.Show("Updating this number will override what's currently saved, continue?", "Update User Phone Number", MessageBoxButtons.OKCancel);
				if (actionUpdateCx == DialogResult.OK) 
				{
					{
						DBControls.UpdateUserNo(txt_UserPhone.Text);
					}
				}
			}
			else
			{
				MessageBox.Show("Unable to updated User Number. Please correct the highlighted fields");
			}

		}

		public const string phoneno = @"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$";
		private bool CheckInput(object sender)
		{

			if (!Regex.IsMatch(txt_UserPhone.Text, phoneno))
			{
				txt_UserPhone.BackColor = Color.FromArgb(138, 138, 138);
				txt_UserPhone.ForeColor = Color.FromArgb(255, 0, 0);
				return notHighlighted = false;
			}
			else 
			{
				txt_UserPhone.BackColor = Color.FromArgb(64, 64, 64);
				txt_UserPhone.ForeColor = Color.SkyBlue;
				notHighlighted = true;
			}
			return notHighlighted;
		}
		private bool EnterField(object field)
		{
			if (field is Control)
			{
				Control thisField = (Control)field;
				thisField.BackColor = Color.FromArgb(64, 64, 64);
				thisField.ForeColor = Color.SkyBlue;
				return true;
			}

			return true;
		}

		private void txt_UserPhone_Leave(object sender, EventArgs e)
		{
			CheckInput(sender);
		}

		private void txt_UserPhone_Enter(object sender, EventArgs e)
		{
			EnterField(sender);
		}
	}
}
