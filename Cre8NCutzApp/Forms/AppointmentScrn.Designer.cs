﻿
namespace Cre8NCutzApp.Forms
{
	partial class AppointmentScrn
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AppointmentScrn));
			this.pnl_Appt = new System.Windows.Forms.Panel();
			this.btn_ApptClear = new System.Windows.Forms.Button();
			this.dd_ApptType = new System.Windows.Forms.ComboBox();
			this.dgv_CurrentAppts = new System.Windows.Forms.DataGridView();
			this.dd_Time = new System.Windows.Forms.ComboBox();
			this.txt_Duration = new System.Windows.Forms.TextBox();
			this.dt_ApptDateTime = new System.Windows.Forms.DateTimePicker();
			this.txt_ApptCxName = new System.Windows.Forms.TextBox();
			this.txt_ApptCxID = new System.Windows.Forms.TextBox();
			this.btn_ApptDelete = new System.Windows.Forms.Button();
			this.btn_ApptNew = new System.Windows.Forms.Button();
			this.btn_ApptUpdate = new System.Windows.Forms.Button();
			this.lbl_Duration = new System.Windows.Forms.Label();
			this.lbl_ApptTime = new System.Windows.Forms.Label();
			this.lbl_ApptDate = new System.Windows.Forms.Label();
			this.lbl_ApptType = new System.Windows.Forms.Label();
			this.lbl_ApptCxID = new System.Windows.Forms.Label();
			this.lbl_ApptCxName = new System.Windows.Forms.Label();
			this.dd_ApptCxList = new System.Windows.Forms.ComboBox();
			this.lbl_CustomerInfo = new System.Windows.Forms.Label();
			this.btn_SearchAppt = new System.Windows.Forms.Button();
			this.pnl_Appt.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgv_CurrentAppts)).BeginInit();
			this.SuspendLayout();
			// 
			// pnl_Appt
			// 
			this.pnl_Appt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.pnl_Appt.Controls.Add(this.btn_SearchAppt);
			this.pnl_Appt.Controls.Add(this.btn_ApptClear);
			this.pnl_Appt.Controls.Add(this.dd_ApptType);
			this.pnl_Appt.Controls.Add(this.dgv_CurrentAppts);
			this.pnl_Appt.Controls.Add(this.dd_Time);
			this.pnl_Appt.Controls.Add(this.txt_Duration);
			this.pnl_Appt.Controls.Add(this.dt_ApptDateTime);
			this.pnl_Appt.Controls.Add(this.txt_ApptCxName);
			this.pnl_Appt.Controls.Add(this.txt_ApptCxID);
			this.pnl_Appt.Controls.Add(this.btn_ApptDelete);
			this.pnl_Appt.Controls.Add(this.btn_ApptNew);
			this.pnl_Appt.Controls.Add(this.btn_ApptUpdate);
			this.pnl_Appt.Controls.Add(this.lbl_Duration);
			this.pnl_Appt.Controls.Add(this.lbl_ApptTime);
			this.pnl_Appt.Controls.Add(this.lbl_ApptDate);
			this.pnl_Appt.Controls.Add(this.lbl_ApptType);
			this.pnl_Appt.Controls.Add(this.lbl_ApptCxID);
			this.pnl_Appt.Controls.Add(this.lbl_ApptCxName);
			this.pnl_Appt.Controls.Add(this.dd_ApptCxList);
			this.pnl_Appt.Controls.Add(this.lbl_CustomerInfo);
			resources.ApplyResources(this.pnl_Appt, "pnl_Appt");
			this.pnl_Appt.Name = "pnl_Appt";
			// 
			// btn_ApptClear
			// 
			this.btn_ApptClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			resources.ApplyResources(this.btn_ApptClear, "btn_ApptClear");
			this.btn_ApptClear.ForeColor = System.Drawing.Color.SkyBlue;
			this.btn_ApptClear.Name = "btn_ApptClear";
			this.btn_ApptClear.UseVisualStyleBackColor = false;
			this.btn_ApptClear.Click += new System.EventHandler(this.btn_ApptClear_Click);
			// 
			// dd_ApptType
			// 
			this.dd_ApptType.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			resources.ApplyResources(this.dd_ApptType, "dd_ApptType");
			this.dd_ApptType.ForeColor = System.Drawing.Color.SkyBlue;
			this.dd_ApptType.FormattingEnabled = true;
			this.dd_ApptType.Items.AddRange(new object[] {
            resources.GetString("dd_ApptType.Items"),
            resources.GetString("dd_ApptType.Items1"),
            resources.GetString("dd_ApptType.Items2"),
            resources.GetString("dd_ApptType.Items3"),
            resources.GetString("dd_ApptType.Items4"),
            resources.GetString("dd_ApptType.Items5"),
            resources.GetString("dd_ApptType.Items6")});
			this.dd_ApptType.Name = "dd_ApptType";
			this.dd_ApptType.Enter += new System.EventHandler(this.dd_ApptType_Enter);
			this.dd_ApptType.Leave += new System.EventHandler(this.dd_ApptType_Leave);
			// 
			// dgv_CurrentAppts
			// 
			this.dgv_CurrentAppts.AllowUserToAddRows = false;
			this.dgv_CurrentAppts.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.dgv_CurrentAppts.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.dgv_CurrentAppts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			resources.ApplyResources(this.dgv_CurrentAppts, "dgv_CurrentAppts");
			this.dgv_CurrentAppts.Name = "dgv_CurrentAppts";
			this.dgv_CurrentAppts.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CurrentAppts_CellClick);
			// 
			// dd_Time
			// 
			this.dd_Time.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			resources.ApplyResources(this.dd_Time, "dd_Time");
			this.dd_Time.ForeColor = System.Drawing.Color.SkyBlue;
			this.dd_Time.FormattingEnabled = true;
			this.dd_Time.Items.AddRange(new object[] {
            resources.GetString("dd_Time.Items"),
            resources.GetString("dd_Time.Items1"),
            resources.GetString("dd_Time.Items2"),
            resources.GetString("dd_Time.Items3"),
            resources.GetString("dd_Time.Items4"),
            resources.GetString("dd_Time.Items5"),
            resources.GetString("dd_Time.Items6"),
            resources.GetString("dd_Time.Items7"),
            resources.GetString("dd_Time.Items8"),
            resources.GetString("dd_Time.Items9"),
            resources.GetString("dd_Time.Items10"),
            resources.GetString("dd_Time.Items11"),
            resources.GetString("dd_Time.Items12"),
            resources.GetString("dd_Time.Items13"),
            resources.GetString("dd_Time.Items14"),
            resources.GetString("dd_Time.Items15"),
            resources.GetString("dd_Time.Items16"),
            resources.GetString("dd_Time.Items17"),
            resources.GetString("dd_Time.Items18"),
            resources.GetString("dd_Time.Items19"),
            resources.GetString("dd_Time.Items20"),
            resources.GetString("dd_Time.Items21"),
            resources.GetString("dd_Time.Items22"),
            resources.GetString("dd_Time.Items23"),
            resources.GetString("dd_Time.Items24"),
            resources.GetString("dd_Time.Items25")});
			this.dd_Time.Name = "dd_Time";
			this.dd_Time.Enter += new System.EventHandler(this.dd_Time_Enter);
			this.dd_Time.Leave += new System.EventHandler(this.dd_Time_Leave);
			// 
			// txt_Duration
			// 
			this.txt_Duration.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.txt_Duration.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			resources.ApplyResources(this.txt_Duration, "txt_Duration");
			this.txt_Duration.ForeColor = System.Drawing.Color.SkyBlue;
			this.txt_Duration.Name = "txt_Duration";
			// 
			// dt_ApptDateTime
			// 
			this.dt_ApptDateTime.CalendarForeColor = System.Drawing.Color.SkyBlue;
			this.dt_ApptDateTime.CalendarMonthBackground = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.dt_ApptDateTime.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			resources.ApplyResources(this.dt_ApptDateTime, "dt_ApptDateTime");
			this.dt_ApptDateTime.Name = "dt_ApptDateTime";
			this.dt_ApptDateTime.Value = new System.DateTime(2022, 5, 29, 0, 0, 0, 0);
			this.dt_ApptDateTime.Enter += new System.EventHandler(this.dt_ApptDateTime_Enter);
			this.dt_ApptDateTime.Leave += new System.EventHandler(this.dt_ApptDateTime_Leave);
			// 
			// txt_ApptCxName
			// 
			this.txt_ApptCxName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.txt_ApptCxName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txt_ApptCxName.ForeColor = System.Drawing.Color.SkyBlue;
			resources.ApplyResources(this.txt_ApptCxName, "txt_ApptCxName");
			this.txt_ApptCxName.Name = "txt_ApptCxName";
			// 
			// txt_ApptCxID
			// 
			this.txt_ApptCxID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.txt_ApptCxID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txt_ApptCxID.ForeColor = System.Drawing.Color.SkyBlue;
			resources.ApplyResources(this.txt_ApptCxID, "txt_ApptCxID");
			this.txt_ApptCxID.Name = "txt_ApptCxID";
			this.txt_ApptCxID.ReadOnly = true;
			this.txt_ApptCxID.MouseMove += new System.Windows.Forms.MouseEventHandler(this.txt_ApptCxID_MouseMove);
			// 
			// btn_ApptDelete
			// 
			this.btn_ApptDelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			resources.ApplyResources(this.btn_ApptDelete, "btn_ApptDelete");
			this.btn_ApptDelete.ForeColor = System.Drawing.Color.SkyBlue;
			this.btn_ApptDelete.Name = "btn_ApptDelete";
			this.btn_ApptDelete.UseVisualStyleBackColor = false;
			this.btn_ApptDelete.Click += new System.EventHandler(this.btn_ApptDelete_Click);
			// 
			// btn_ApptNew
			// 
			this.btn_ApptNew.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			resources.ApplyResources(this.btn_ApptNew, "btn_ApptNew");
			this.btn_ApptNew.ForeColor = System.Drawing.Color.SkyBlue;
			this.btn_ApptNew.Name = "btn_ApptNew";
			this.btn_ApptNew.UseVisualStyleBackColor = false;
			this.btn_ApptNew.Click += new System.EventHandler(this.btn_ApptNew_Click);
			// 
			// btn_ApptUpdate
			// 
			this.btn_ApptUpdate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			resources.ApplyResources(this.btn_ApptUpdate, "btn_ApptUpdate");
			this.btn_ApptUpdate.ForeColor = System.Drawing.Color.SkyBlue;
			this.btn_ApptUpdate.Name = "btn_ApptUpdate";
			this.btn_ApptUpdate.UseVisualStyleBackColor = false;
			this.btn_ApptUpdate.Click += new System.EventHandler(this.btn_ApptUpdate_Click);
			// 
			// lbl_Duration
			// 
			resources.ApplyResources(this.lbl_Duration, "lbl_Duration");
			this.lbl_Duration.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.lbl_Duration.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(63)))), ((int)(((byte)(150)))));
			this.lbl_Duration.Name = "lbl_Duration";
			// 
			// lbl_ApptTime
			// 
			resources.ApplyResources(this.lbl_ApptTime, "lbl_ApptTime");
			this.lbl_ApptTime.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.lbl_ApptTime.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(63)))), ((int)(((byte)(150)))));
			this.lbl_ApptTime.Name = "lbl_ApptTime";
			// 
			// lbl_ApptDate
			// 
			resources.ApplyResources(this.lbl_ApptDate, "lbl_ApptDate");
			this.lbl_ApptDate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.lbl_ApptDate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(63)))), ((int)(((byte)(150)))));
			this.lbl_ApptDate.Name = "lbl_ApptDate";
			// 
			// lbl_ApptType
			// 
			resources.ApplyResources(this.lbl_ApptType, "lbl_ApptType");
			this.lbl_ApptType.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.lbl_ApptType.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(63)))), ((int)(((byte)(150)))));
			this.lbl_ApptType.Name = "lbl_ApptType";
			// 
			// lbl_ApptCxID
			// 
			resources.ApplyResources(this.lbl_ApptCxID, "lbl_ApptCxID");
			this.lbl_ApptCxID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.lbl_ApptCxID.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(63)))), ((int)(((byte)(150)))));
			this.lbl_ApptCxID.Name = "lbl_ApptCxID";
			// 
			// lbl_ApptCxName
			// 
			resources.ApplyResources(this.lbl_ApptCxName, "lbl_ApptCxName");
			this.lbl_ApptCxName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.lbl_ApptCxName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(63)))), ((int)(((byte)(150)))));
			this.lbl_ApptCxName.Name = "lbl_ApptCxName";
			// 
			// dd_ApptCxList
			// 
			this.dd_ApptCxList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.dd_ApptCxList.DisplayMember = "CustName";
			resources.ApplyResources(this.dd_ApptCxList, "dd_ApptCxList");
			this.dd_ApptCxList.ForeColor = System.Drawing.Color.SkyBlue;
			this.dd_ApptCxList.FormattingEnabled = true;
			this.dd_ApptCxList.Name = "dd_ApptCxList";
			this.dd_ApptCxList.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.Dd_ApptCxList_DrawItem);
			this.dd_ApptCxList.SelectedIndexChanged += new System.EventHandler(this.dd_ApptCxList_SelectedIndexChanged);
			this.dd_ApptCxList.DropDownClosed += new System.EventHandler(this.Dd_ApptCxList_DropDownClosed);
			// 
			// lbl_CustomerInfo
			// 
			resources.ApplyResources(this.lbl_CustomerInfo, "lbl_CustomerInfo");
			this.lbl_CustomerInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.lbl_CustomerInfo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(63)))), ((int)(((byte)(150)))));
			this.lbl_CustomerInfo.Name = "lbl_CustomerInfo";
			// 
			// btn_SearchAppt
			// 
			this.btn_SearchAppt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			resources.ApplyResources(this.btn_SearchAppt, "btn_SearchAppt");
			this.btn_SearchAppt.ForeColor = System.Drawing.Color.SkyBlue;
			this.btn_SearchAppt.Name = "btn_SearchAppt";
			this.btn_SearchAppt.UseVisualStyleBackColor = false;
			this.btn_SearchAppt.Click += new System.EventHandler(this.btn_SearchAppt_Click);
			// 
			// AppointmentScrn
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.Controls.Add(this.pnl_Appt);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "AppointmentScrn";
			this.Load += new System.EventHandler(this.AppointmentScrn_Load);
			this.pnl_Appt.ResumeLayout(false);
			this.pnl_Appt.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgv_CurrentAppts)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel pnl_Appt;
		private System.Windows.Forms.ComboBox dd_Time;
		private System.Windows.Forms.TextBox txt_Duration;
		private System.Windows.Forms.DateTimePicker dt_ApptDateTime;
		private System.Windows.Forms.TextBox txt_ApptCxName;
		private System.Windows.Forms.TextBox txt_ApptCxID;
		private System.Windows.Forms.Button btn_ApptDelete;
		private System.Windows.Forms.Button btn_ApptNew;
		private System.Windows.Forms.Button btn_ApptUpdate;
		private System.Windows.Forms.Label lbl_Duration;
		private System.Windows.Forms.Label lbl_ApptTime;
		private System.Windows.Forms.Label lbl_ApptDate;
		private System.Windows.Forms.Label lbl_ApptType;
		private System.Windows.Forms.Label lbl_ApptCxID;
		private System.Windows.Forms.Label lbl_ApptCxName;
		private System.Windows.Forms.Label lbl_CustomerInfo;
		private System.Windows.Forms.DataGridView dgv_CurrentAppts;
		public System.Windows.Forms.ComboBox dd_ApptCxList;
		private System.Windows.Forms.ComboBox dd_ApptType;
		private System.Windows.Forms.Button btn_ApptClear;
		private System.Windows.Forms.Button btn_SearchAppt;
	}
}