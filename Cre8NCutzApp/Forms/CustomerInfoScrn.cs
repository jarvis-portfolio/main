﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Cre8NCutzApp.Controllers;
using Cre8NCutzApp.Models;
using Cre8NCutzApp.Forms;
using MySql.Data.MySqlClient;
using System.Text.RegularExpressions;

namespace Cre8NCutzApp.Forms
{
	//
	//   - For this screen to work, The user has to first select from the
	// dropdown that's populated through the database call to grab just 
	// the Customer names.
	// Then, I need to take the customer name selected and
	// grab the table row and column that has
	// the customer name that matches, get that customer row's data, then I need
	// to get the data matching/the ids from the other tables (data and dataid)
	// Take the data that comes out and assign it to its own constructor, taking
	// in each row and column, that matches the data type and assign it to its own
	// object within that class.
	// Then display the findings on the screen for the user.
	// Then setup the event changer on btn_update click to : first validate the fields
	// that data was inserted into, then assign the data to fields where the data is
	// being updated that was selected the set the selected.
	// Delete should be made to take the selected and delete at the row found and
	// all associated ids matching.
	//
	//
	public partial class CustomerInfoScrn : Form
	{
		
		public static CustomerInfoScrn CXInfo;
		public string constring = DBConnect.OpenDB();
		ToolTip cxListTT = new ToolTip();
		bool notHighlighted = true;
		bool selectedCustomer = false;

		delegate bool del_TextEmpty(Control field);
		del_TextEmpty textEmpty = f => f == null || f.IsDisposed || f.Text.Length == 0;
		// Using the lambda expression above to perform null checks on the controls for field validation
		// Saves multiple lines of code for the same process without creating a full function to check for nulls
		delegate bool del_ComboEmpty(ComboBox field);
		del_ComboEmpty comboEmpty = f => f == null || f.IsDisposed || f.SelectedIndex == -1;
		// Using a lambda expression above to perform null checks on the comboBoxes for field validation
		// Saves multiple lines of code. Same as above but different for control type comboBoxes as the check is mainly for selectedindex -1		



		public CustomerInfoScrn()
		{
			InitializeComponent();
		}

		private void CustomerInfoScrn_Load(object sender, EventArgs e)
		{
			pnl_CxInfo.BackColor = Color.FromArgb(25, 64, 64, 64);
			ChooseCustomer();
			dd_CustomerList.DrawMode = DrawMode.OwnerDrawFixed;
			dd_CustomerList.DrawItem -= Dd_CustomerList_DrawItem;
			dd_CustomerList.DrawItem += Dd_CustomerList_DrawItem;
			dd_CustomerList.DropDownClosed -= Dd_CustomerList_DropDownClosed;
			dd_CustomerList.DropDownClosed += Dd_CustomerList_DropDownClosed;
			
		}

		private void Dd_CustomerList_DropDownClosed(object sender, EventArgs e)
		{
			cxListTT.Hide(dd_CustomerList);
		}

		private void Dd_CustomerList_DrawItem(object sender, DrawItemEventArgs e)
		{
			if (e.Index < 0)
			{
				return;
			}
			string ccHoverText = dd_CustomerList.GetItemText(dd_CustomerList.Items[e.Index]);
			e.DrawBackground();
			using (SolidBrush br = new SolidBrush(e.ForeColor))
			{
				e.Graphics.DrawString(ccHoverText, e.Font, br, e.Bounds);
			}
			if ((e.State & DrawItemState.Selected) == DrawItemState.Selected)
			{
				cxListTT.Show(ccHoverText, dd_CustomerList, e.Bounds.Right, e.Bounds.Bottom);
			}
			e.DrawFocusRectangle();
		}

		

		private void dd_CustomerList_SelectedIndexChanged(object sender, EventArgs e)
		{
			// Grab the dd_CusomterList.SelectedItem and search the customer table for matching customerName and grab the
			// matching the customerId and return data from customer table (customerId, customerName, addressId) then addressId to search
			 


			CustomerModel cxModel = (CustomerModel) dd_CustomerList.SelectedItem;
			txt_CxID.Text = cxModel.CustId.ToString();
			txt_CxName.Text = cxModel.CustName;
			txt_CxAddress.Text = cxModel.CustAddr;
			txt_CxPhone.Text = cxModel.CustAddrPhone;
			txt_CxCity.Text = cxModel.CustCityName;
			txt_CxCountry.Text = cxModel.CustCountryName;
			selectedCustomer = true;
			FieldCheck();

		}
		public void ChooseCustomer()
		{
			if (dd_CustomerList.Items.Count < 1)
			{
				DBControls.ChooseCustomer(ref dd_CustomerList);
			}
			else
			{
				dd_CustomerList.Items.Clear();
				DBControls.ChooseCustomer(ref dd_CustomerList);
			}
			
			
		}

		private void dd_CustomerList_MouseHover(object sender, EventArgs e)
		{
			if (dd_CustomerList != null)
			{
				//ToolTip cxListTT = new ToolTip();
				//cxListTT.SetToolTip(dd_CustomerList., "first customer testing");
				//cxListTT.AutoPopDelay = 5000;
				//cxListTT.InitialDelay = 1000;
				//cxListTT.ReshowDelay = 500;
				//cxListTT.ShowAlways = true;
			}
			
		}
		

		private void btn_CxClear_Click(object sender, EventArgs e)
		{
			//  - Clear textbox fields and set dropdown back to Choose Customer. No Selected Items and DataSource to Null
			DialogResult actionAddNewCx = MessageBox.Show("Clear the information and make no changes?", "Clear Customer Information", MessageBoxButtons.YesNo);
			if (actionAddNewCx == DialogResult.Yes)
			{
				ClearFields();

			}
		}
		private void btn_CxNew_Click(object sender, EventArgs e)
		{
			FieldCheck();


			if (notHighlighted)
			{
				//  - Need to check the fields that have no data or have not had focus. if they are empty on click then fail.
				CustomerModel cm;
				DialogResult actionAddNewCx = MessageBox.Show("Would you like to add this customer?", "Add New Customer", MessageBoxButtons.YesNo);
				if (actionAddNewCx == DialogResult.Yes)
				{
					if(txt_CxID.Text == string.Empty)
					{
						cm = new CustomerModel
						{
							CustId = 0,
							CustName = txt_CxName.Text,
							CustAddr = txt_CxAddress.Text,
							CustAddrPhone = txt_CxPhone.Text,
							CustCityName = txt_CxCity.Text,
							CustCountryName = txt_CxCountry.Text

						};
						if (cm != null)
						{
							DBControls.NewCustomer(cm);
							ClearFields();
						}
					}
					else
					{
						MessageBox.Show("Unable to add new customer. Customer already exists. Please choose update.");
					}
				}
			}
			else
			{
				MessageBox.Show("Unable to add new customer. Please correct the highlighted fields");
			}

		}

		private void btn_CxUpdate_Click(object sender, EventArgs e)
		{
			FieldCheck();
			if (selectedCustomer)
			{
				if (notHighlighted)
				{
					CustomerModel cm;
					DialogResult actionUpdateCx = MessageBox.Show("Updating this customer will override what's currently saved, continue?", "Update Customer", MessageBoxButtons.OKCancel);
					if (actionUpdateCx == DialogResult.OK) //  - *tag - repetitive make into function
					{
						cm = new CustomerModel
						{
							CustId = Int32.Parse(txt_CxID.Text), 
							CustName = txt_CxName.Text,
							CustAddr = txt_CxAddress.Text,
							CustAddrPhone = txt_CxPhone.Text,
							CustCityName = txt_CxCity.Text,
							CustCountryName = txt_CxCountry.Text
						};
						if (cm != null)
						{
							DBControls.UpdateCustomer(cm);
							ClearFields();
						}
					}
				}
				else
				{
					MessageBox.Show("Unable to updated customer. Please correct the highlighted fields");
				}
			}
			else
			{
				MessageBox.Show("Unable to updated customer. Please correct the highlighted fields");
				dd_CustomerList.BackColor = Color.FromArgb(138, 138, 138);
			}
			
		}

		private void btn_CxDelete_Click(object sender, EventArgs e)
		{
			FieldCheck();

			if (notHighlighted && selectedCustomer)
			{
				CustomerModel cm;
				DialogResult actionDeleteCx = MessageBox.Show("Would you like to completely remove this customer?", "Delete Customer", MessageBoxButtons.YesNo);
				if (actionDeleteCx == DialogResult.Yes)
				{
					cm = new CustomerModel
					{
						CustId = Int32.Parse(txt_CxID.Text), 
						CustName = txt_CxName.Text,
						CustAddr = txt_CxAddress.Text,
						CustAddrPhone = txt_CxPhone.Text,
						CustCityName = txt_CxCity.Text,
						CustCountryName = txt_CxCountry.Text
					};
					if (cm != null)
					{
						DBControls.DeleteCustomer(cm);
						ClearFields();
					}
				}
			}
			else
			{
				MessageBox.Show("Please select a customer to be deleted.");
				dd_CustomerList.BackColor = Color.FromArgb(138, 138, 138);
			}
			
		}

		private void ClearFields()
		{

			txt_CxID.Text = "";
			txt_CxID.BackColor = Color.FromArgb(64, 64, 64);
			txt_CxID.ForeColor = Color.SkyBlue;
			txt_CxName.Text = "";
			txt_CxName.BackColor = Color.FromArgb(64, 64, 64);
			txt_CxName.ForeColor = Color.SkyBlue;
			txt_CxAddress.Text = "";
			txt_CxAddress.BackColor = Color.FromArgb(64, 64, 64);
			txt_CxAddress.ForeColor = Color.SkyBlue;
			txt_CxPhone.Text = "";
			txt_CxPhone.BackColor = Color.FromArgb(64, 64, 64);
			txt_CxPhone.ForeColor = Color.SkyBlue;
			txt_CxCity.Text = "";
			txt_CxCity.BackColor = Color.FromArgb(64, 64, 64);
			txt_CxCity.ForeColor = Color.SkyBlue;
			txt_CxCountry.Text = "";
			txt_CxCountry.BackColor = Color.FromArgb(64, 64, 64);
			txt_CxCountry.ForeColor = Color.SkyBlue;
			dd_CustomerList.Text = "Choose Customer";
			dd_CustomerList.BackColor = Color.FromArgb(64, 64, 64);
			dd_CustomerList.ForeColor = Color.SkyBlue;
			dd_CustomerList.DataSource = null;
			ChooseCustomer();


		}

		private void FieldCheck()
		{
			
			List<TextBox> cxText = new List<TextBox>()
			{
				txt_CxName,
				txt_CxAddress,
				txt_CxPhone,
				txt_CxCity,
				txt_CxCountry
			};

			foreach (TextBox field in cxText)
			{
				notHighlighted = true;
				CheckInput(field);
				if (notHighlighted == false) return;
			}

			
		}
		public const string phoneno = @"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$";
		private bool CheckInput(object field)
		{
			
			if (!Regex.IsMatch(txt_CxPhone.Text, phoneno))
			{
				txt_CxPhone.BackColor = Color.FromArgb(138, 138, 138);
				txt_CxPhone.ForeColor = Color.FromArgb(255, 0, 0);
				return notHighlighted = false;
			}
			else if (textEmpty(field as Control) && field is TextBox)
			{
				TextBox textField = (TextBox)field;
				textField.BackColor = Color.FromArgb(138, 138, 138);
				textField.ForeColor = Color.FromArgb(255, 0, 0);
				return notHighlighted = false;
			}
			else if (field is TextBox)
			{
				TextBox textField = (TextBox)field;
				textField.BackColor = Color.FromArgb(64, 64, 64);
				textField.ForeColor = Color.SkyBlue;
				return notHighlighted = true;
			}
			else if (comboEmpty(field as ComboBox) && field is ComboBox)
			{
				ComboBox ddField = (ComboBox)field;
				ddField.BackColor = Color.FromArgb(138, 138, 138);
				ddField.ForeColor = Color.FromArgb(255, 0, 0);
				return notHighlighted = false;
			}
			else if (field is ComboBox)
			{
				ComboBox ddField = (ComboBox)field;
				ddField.BackColor = Color.FromArgb(64, 64, 64);
				ddField.ForeColor = Color.SkyBlue;
				return notHighlighted = true;
			}

			return notHighlighted;
		}

		private bool EnterField(object field)
		{
			if (field is Control)
			{
				Control thisField = (Control)field;
				thisField.BackColor = Color.FromArgb(64, 64, 64);
				thisField.ForeColor = Color.SkyBlue;
				return true;
			}
			
			return true;
		}

		
		private void txt_CxName_Leave(object sender, EventArgs e)
		{
			CheckInput(sender);
		}

		private void txt_CxName_Enter(object sender, EventArgs e)
		{
			EnterField(sender);
		}

		private void txt_CxAddress_Leave(object sender, EventArgs e)
		{
			CheckInput(sender);
		}

		private void txt_CxAddress_Enter(object sender, EventArgs e)
		{
			EnterField(sender);
		}

		private void txt_CxPhone_Leave(object sender, EventArgs e)
		{
			CheckInput(sender);
		}

		private void txt_CxPhone_Enter(object sender, EventArgs e)
		{
			EnterField(sender);
		}

		private void txt_CxCity_Leave(object sender, EventArgs e)
		{
			CheckInput(sender);
		}

		private void txt_CxCity_Enter(object sender, EventArgs e)
		{
			EnterField(sender);
		}

		private void txt_CxCountry_Leave(object sender, EventArgs e)
		{
			CheckInput(sender);
		}

		private void txt_CxCountry_Enter(object sender, EventArgs e)
		{
			EnterField(sender);
		}

		private void dd_CustomerList_Leave(object sender, EventArgs e)
		{
			CheckInput(sender);
		}

		private void dd_CustomerList_Enter(object sender, EventArgs e)
		{
			EnterField(sender);
		}
		
	}
}
