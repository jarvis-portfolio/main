﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Cre8NCutzApp.Controllers;
using Cre8NCutzApp.Models;
using MySql.Data.MySqlClient;


namespace Cre8NCutzApp.Forms
{
	//  - For this screen the user should be able to pick from the list of customers
	// Like before and then have each data point populate in the text boxes
	// Also once a user is selected I want to populate a grid with the Customer's
	// Appointment type and DateTime combined
	// Add new should update the appointment record with the appropriate fields and properties
	// From the constructor of the ViewModel to the constructor of the DBModel with its
	// matching ids to update other tables
	// The update should take the selectedValue and record on the database
	// and the record that has the same customer id and customer name attached to the
	// appointment and update.
	// The Delete should remove the record from the database
	// Appointments should have check for when the correct data is inserted for Names, which
	// shouldn't be changed, if there's a select value and the name changed and user hits update,
	// instead of graying it out.

	public partial class AppointmentScrn : Form
	{
		public string constring = DBConnect.OpenDB();
		ToolTip cxListTT = new ToolTip();
		public static AppointmentScrn Appts;
		bool notHighlighted = true;
		DataTable apptGrid = new DataTable();
		ToolTip apptIDTextBox = new ToolTip();
		delegate bool del_TextEmpty(Control field);
		del_TextEmpty textEmpty = f => f == null || f.IsDisposed || f.Text.Length == 0;
		// Using the lambda expression above to perform null checks on the controls for field validation
		// Saves multiple lines of code for the same process without creating a full function to check for nulls
		delegate bool del_ComboEmpty(ComboBox field);
		del_ComboEmpty comboEmpty = f => f == null || f.IsDisposed || f.SelectedIndex == -1;
		// Using a lambda expression above to perform null checks on the comboBoxes for field validation
		// Saves multiple lines of code. Same as above but different for control type comboBoxes as the check is mainly for selectedindex -1

		public AppointmentScrn()
		{
			InitializeComponent();
			dd_ApptCxList.DrawMode = DrawMode.OwnerDrawFixed;
			
		}

		private void Dd_ApptCxList_DropDownClosed(object sender, EventArgs e)
		{
			cxListTT.Hide(dd_ApptCxList);
			
		}

		private void Dd_ApptCxList_DrawItem(object sender, DrawItemEventArgs e)
		{
			if (e.Index < 0)
			{
				return;
			}
			string ccHoverText = dd_ApptCxList.GetItemText(dd_ApptCxList.Items[e.Index]);
			e.DrawBackground();
			using (SolidBrush br = new SolidBrush(e.ForeColor))
			{
				e.Graphics.DrawString(ccHoverText, e.Font, br, e.Bounds);
			}
			if ((e.State & DrawItemState.Selected) == DrawItemState.Selected)
			{
				cxListTT.Show(ccHoverText, dd_ApptCxList, e.Bounds.Right, e.Bounds.Bottom);
			}
			e.DrawFocusRectangle();
		}

		private void AppointmentScrn_Load(object sender, EventArgs e)
		{
			ChooseCustomer();

		}
		private void dd_ApptCxList_SelectedIndexChanged(object sender, EventArgs e) 
		{
			CustomerModel cxModel = (CustomerModel)dd_ApptCxList.SelectedItem;

			txt_ApptCxID.Text = cxModel.CustId.ToString();
			txt_ApptCxName.Text = cxModel.CustName;
			dd_ApptType.Text = string.Empty;
			dt_ApptDateTime.Text = string.Empty;
			dd_Time.SelectedValue = string.Empty;

			//if (cxModel.CustId > 0)
			//{
			//	apptGrid.Rows.Clear();
			//	DBControls.ApptGrid(apptGrid, cxModel.CustId); 
			//}


			//dgv_CurrentAppts.DataSource = apptGrid; 
			//formatDGV_CurrentAppts(dgv_CurrentAppts);
		}

		private void formatDGV_CurrentAppts(DataGridView e)
		{
			//e.BorderStyle = BorderStyle.Fixed3D;
			e.DefaultCellStyle.BackColor = Color.FromArgb(64, 64, 64);
			e.DefaultCellStyle.ForeColor = Color.SkyBlue;
			e.RowHeadersVisible = false;
			e.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
			e.Columns[0].Visible = false;
			e.Columns[1].Visible = false;
			e.Columns[2].Visible = false;
			e.Columns[3].Visible = false;
			e.Columns[4].HeaderText = "Type";
			e.Columns[5].HeaderText = "Start Time"; 
			e.Columns[5].DefaultCellStyle.Format = "HH:mm";
			dgv_CurrentAppts.Font = new Font("Microsoft Sans Serif", 8);
			
			e.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
			e.ReadOnly = true;

		}


		public void ChooseCustomer()
		{
			DBControls.ChooseCustomer(ref dd_ApptCxList);
			
		}
		private void dgv_CurrentAppts_CellClick(object sender, DataGridViewCellEventArgs e)
		{ 
			AppointmentModel apptCx = new AppointmentModel();
			if (dgv_CurrentAppts.Rows[e.RowIndex].Cells[e.ColumnIndex].Value != null)
			{
				foreach (DataGridViewColumn column in dgv_CurrentAppts.Columns)
				{
					apptCx.ApptID = Convert.ToInt32(dgv_CurrentAppts.SelectedRows[0].Cells[0].Value); 
					apptCx.CustName = dgv_CurrentAppts.SelectedRows[0].Cells[1].Value.ToString(); 
					apptCx.CustId = Convert.ToInt32(dgv_CurrentAppts.SelectedRows[0].Cells[2].Value); 
					apptCx.UserID = Convert.ToInt32(dgv_CurrentAppts.SelectedRows[0].Cells[3].Value); 
					apptCx.ApptType = dgv_CurrentAppts.SelectedRows[0].Cells[4].Value.ToString(); 
					apptCx.ApptStartTime = Convert.ToDateTime(dgv_CurrentAppts.SelectedRows[0].Cells[5].Value);
				}
				if (apptCx != null)
				{
					dd_ApptType.Text = apptCx.ApptType.ToString();
					dt_ApptDateTime.Text = apptCx.ApptStartTime.ToString("MM/dd/yyyy");
					dd_Time.Text = apptCx.ApptStartTime.ToString("HH:mm");
				}
			}
			else
			{
				return;
			}
			
		}
		
		private void ClearFields()
		{
			dd_ApptCxList.Text = "Choose Customer";
			dd_ApptCxList.BackColor = Color.FromArgb(64, 64, 64);
			dd_ApptCxList.ForeColor = Color.SkyBlue;
			txt_ApptCxID.Text = "";
			txt_ApptCxID.BackColor = Color.FromArgb(64, 64, 64);
			txt_ApptCxID.ForeColor = Color.SkyBlue;
			txt_ApptCxName.Text = "";
			txt_ApptCxName.BackColor = Color.FromArgb(64, 64, 64);
			txt_ApptCxName.ForeColor = Color.SkyBlue;
			dd_ApptType.Text = "";
			dd_ApptType.BackColor = Color.FromArgb(64, 64, 64);
			dd_ApptType.ForeColor = Color.SkyBlue;
			dt_ApptDateTime.Text = "";
			dt_ApptDateTime.BackColor = Color.FromArgb(64, 64, 64);
			dt_ApptDateTime.ForeColor = Color.SkyBlue;
			dd_Time.Text = "";
			dd_Time.BackColor = Color.FromArgb(64, 64, 64);
			dd_Time.ForeColor = Color.SkyBlue;
			dgv_CurrentAppts.DataSource = null;
			dgv_CurrentAppts.Rows.Clear();

		}
		private void btn_ApptClear_Click(object sender, EventArgs e)
		{
			DialogResult actionAddNewCx = MessageBox.Show("Clear the information and make no changes?", "Clear Appointment Information", MessageBoxButtons.YesNo);
			if (actionAddNewCx == DialogResult.Yes)
			{
				ClearFields();
			}
		}

		private void btn_ApptNew_Click(object sender, EventArgs e)
		{
			FieldCheck();

			if (notHighlighted)
			{
				//  - Need to check the fields that have no data or have not had focus. if they are empty on click then fail.
				
				AppointmentModel apptCx = new AppointmentModel();
				DialogResult actionAddNewAppt = MessageBox.Show("Would you like to add a new appointment for this customer?", "Add New Appointment", MessageBoxButtons.YesNo);
				if (actionAddNewAppt == DialogResult.Yes)
				{
					if (dd_ApptCxList.SelectedIndex != -1) 
					{

						apptCx.CustName = txt_ApptCxName.Text; 
						apptCx.CustId = Convert.ToInt32(txt_ApptCxID.Text); 
						apptCx.ApptType = dd_ApptType.Text; 
						try
						{
							DateTime newApptStartDate = dt_ApptDateTime.Value;
							DateTime newApptStartTime = DateTime.Parse(dd_Time.Text);
							if (newApptStartTime.Hour >= 17 && newApptStartTime.Minute >= 0 || newApptStartTime.Hour < 8)
							{
								MessageBox.Show("Appointment is outside business hours.");
								return;
							}
							DateTime newApptStart = new DateTime(newApptStartDate.Year, newApptStartDate.Month, newApptStartDate.Day,
							newApptStartTime.Hour, newApptStartTime.Minute, newApptStartTime.Second); 
							apptCx.ApptStartTime = newApptStart.ToUniversalTime();
						}
						catch
						{
							MessageBox.Show(" Unable to parse... " + dd_Time.SelectedItem.ToString());
						}
					}
					else
					{
						MessageBox.Show("Failed...");
					}
					if (apptCx != null)
					{
						DBControls.NewAppointment(apptCx);
						ClearFields();

					}
				}
			}
			else
			{
				MessageBox.Show("Unable to add new customer. Please correct the highlighted fields");
			}
		}

		private void btn_ApptUpdate_Click(object sender, EventArgs e)
		{
			FieldCheck();

			if (notHighlighted && dd_ApptCxList.SelectedIndex != -1)
			{
				//  - Need to check the fields that have no data or have not had focus. if they are empty on click then fail.
				AppointmentModel apptCx = new AppointmentModel();
				DialogResult actionUpdateAppt = MessageBox.Show("Updating this appointment will override what's currently saved, continue?", "Update Appointment", MessageBoxButtons.OKCancel);
				if (actionUpdateAppt == DialogResult.OK)
				{
					if (dgv_CurrentAppts.SelectedRows != null)
					{
						apptCx.ApptID = Convert.ToInt32(dgv_CurrentAppts.SelectedRows[0].Cells[0].Value);
						apptCx.CustName = dgv_CurrentAppts.SelectedRows[0].Cells[1].Value.ToString();
						apptCx.CustId = Convert.ToInt32(dgv_CurrentAppts.SelectedRows[0].Cells[2].Value);
						apptCx.UserID = Convert.ToInt32(dgv_CurrentAppts.SelectedRows[0].Cells[3].Value);
						apptCx.ApptType = dd_ApptType.Text;
						try
						{
							DateTime newApptStartDate = dt_ApptDateTime.Value;
							DateTime newApptStartTime = DateTime.Parse(dd_Time.Text);
							//MessageBox.Show(newApptStartTime.Hour.ToString());

							if (newApptStartTime.Hour >= 17 && newApptStartTime.Minute >= 0 || newApptStartTime.Hour < 8)
							{
								MessageBox.Show("Appointment is outside business hours.");
								return;
							}
							DateTime newApptStart = new DateTime(newApptStartDate.Year, newApptStartDate.Month, newApptStartDate.Day,
							newApptStartTime.Hour, newApptStartTime.Minute, newApptStartTime.Second); //.ToLocalTime()
							apptCx.ApptStartTime = newApptStart.ToUniversalTime();
						}
						catch
						{
							MessageBox.Show(" Unable to parse... " + dd_Time.SelectedItem.ToString());
						}
					}
					else
					{
						MessageBox.Show("No appointment to update.");
						return;
					}
					if (apptCx != null)
					{
						DBControls.UpdateAppointment(apptCx);
						ClearFields();

					}
				}
			}
			else
			{
				MessageBox.Show("Unable to add update appointment. Please enter valid appointment to update.");
			}
		}

		private void btn_ApptDelete_Click(object sender, EventArgs e)
		{
			FieldCheck();
			if (notHighlighted)
			{
				//  - Need to check the fields that have no data or have not had focus. if they are empty on click then fail.
				AppointmentModel apptCx = new AppointmentModel();
				DialogResult actionDeleteAppt = MessageBox.Show("Would you like to completely remove this appointment?", "Delete Appointment", MessageBoxButtons.YesNo);
				if (actionDeleteAppt == DialogResult.Yes)
				{
					if (dgv_CurrentAppts.SelectedRows != null)
					{
						apptCx.ApptID = Convert.ToInt32(dgv_CurrentAppts.SelectedRows[0].Cells[0].Value);
						apptCx.CustName = dgv_CurrentAppts.SelectedRows[0].Cells[1].Value.ToString();
						apptCx.CustId = Convert.ToInt32(dgv_CurrentAppts.SelectedRows[0].Cells[2].Value);
						apptCx.UserID = Convert.ToInt32(dgv_CurrentAppts.SelectedRows[0].Cells[3].Value);
						apptCx.ApptType = dd_ApptType.Text;

					}
					else
					{
						MessageBox.Show("No appointment to delete.");
						return;
					}
					if (apptCx != null)
					{
						DBControls.DeleteAppointment(apptCx);
						ClearFields();
						
					}
				}
			}
			else
			{
				MessageBox.Show("Unable to remove appointment. Please enter valid appointment to remove.");
			}
		}

		private void FieldCheck()
		{
			List<Control> apptText = new List<Control>()
			{

				dd_ApptCxList,
				txt_ApptCxName,
				dd_ApptType,
				dt_ApptDateTime,
				dd_Time
			};

			foreach (Control field in apptText)
			{
				notHighlighted = true;
				CheckInput(field);
				if (notHighlighted == false) return;
			}
		}
		private bool EnterField(object field)
		{
			if (field is Control)
			{
				Control thisField = (Control)field;
				thisField.BackColor = Color.FromArgb(64, 64, 64);
				thisField.ForeColor = Color.SkyBlue;
				return true;
			}

			return true;
		}

		private bool CheckInput(object field)
		{


			if (textEmpty(field as Control) && field is TextBox)
			{
				TextBox textField = (TextBox)field;
				textField.BackColor = Color.FromArgb(138, 138, 138);
				textField.ForeColor = Color.FromArgb(255, 0, 0);
				return notHighlighted = false;
			}
			else if (field is TextBox)
			{
				TextBox textField = (TextBox)field;
				textField.BackColor = Color.FromArgb(64, 64, 64);
				textField.ForeColor = Color.SkyBlue;
				return notHighlighted = true;
			}


			if (comboEmpty(field as ComboBox) && field is ComboBox)
			{
				ComboBox ddField = (ComboBox)field;
				ddField.BackColor = Color.FromArgb(138, 138, 138);
				ddField.ForeColor = Color.FromArgb(255, 0, 0);
				return notHighlighted = false;
			}
			else if (field is ComboBox)
			{
				ComboBox ddField = (ComboBox)field;
				ddField.BackColor = Color.FromArgb(64, 64, 64);
				ddField.ForeColor = Color.SkyBlue;
				return notHighlighted = true;
			}


			return notHighlighted;
		}

		private void dd_ApptType_Enter(object sender, EventArgs e)
		{
			EnterField(sender);
		}

		private void dd_ApptType_Leave(object sender, EventArgs e)
		{
			CheckInput(sender);
			
		}

		private void dt_ApptDateTime_Enter(object sender, EventArgs e)
		{

		}

		private void dt_ApptDateTime_Leave(object sender, EventArgs e)
		{
			CheckInput(sender);
		}

		private void dd_Time_Enter(object sender, EventArgs e)
		{
			EnterField(sender);
		}

		private void dd_Time_Leave(object sender, EventArgs e)
		{
			CheckInput(sender);
		}

		private void txt_ApptCxID_MouseMove(object sender, MouseEventArgs e)
		{
			apptIDTextBox.SetToolTip(txt_ApptCxID, "Read-Only.");
		}

		private void btn_SearchAppt_Click(object sender, EventArgs e)
		{
			if (txt_ApptCxID.Text != "")
			{
				apptGrid.Rows.Clear();
				DBControls.ApptGrid(apptGrid, Int32.Parse(txt_ApptCxID.Text));
			}

			dgv_CurrentAppts.DataSource = apptGrid;
			formatDGV_CurrentAppts(dgv_CurrentAppts);
		}
	}
}
