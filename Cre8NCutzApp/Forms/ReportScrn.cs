﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Cre8NCutzApp.Controllers;


namespace Cre8NCutzApp.Forms
{
	// - 3rd report should be the Number of times a user has logged
	// in, chronologically by the Login clicks, by the day
	// It should display and also have an option for downloading reports
	// So create a button for download btn_DownloadReport that opens .txt file with the data
	// txt file should show private int _UserID = 0;
	// username,UserID UserActive List of functions performed
	// Add, Update or Delete and from Appointment or Customer and time stamps for each function
	// |Test2 |2  |InActive|None  |None        |None        |None      |None    |
	// |test  |1  |Active  |Update|Appointment |Peter Parker|01/29/2020|19:35CST|
	// |test  |1  |Active  |Delete|Customer    |Peter Parker|01/29/2020|19:42CST|
	// Each button click needs to also capture this data from the function
	
	


	public partial class ReportScrn : Form
	{
		DataTable apptTypeGrid = new DataTable();
		DataTable userSchGrid = new DataTable();
		DataTable numCxGrid = new DataTable();
		DateTime currentDate = DateTime.Now;
		DateTime currMonth;
		public ReportScrn()
		{
			InitializeComponent();
		}

		private void dd_ReportList_SelectedIndexChanged(object sender, EventArgs e)
		{
			dd_ByUser.Visible = false;
			dd_ByMonth.Visible = false;
			dd_ByType.Visible = false;
			dd_ByUser.Text = "By User";
			dd_ByMonth.Text = "By Month";
			currMonth = currentDate;
			dd_ByType.Text = "By Type";

			if ("Appointment Types - This Month".Equals(dd_ReportList.SelectedItem))
			{
				apptTypeGrid.Rows.Clear();
				apptType_formatDGV_ReportsView(currentDate);

			}
			else if ("Schedule for User".Equals(dd_ReportList.SelectedItem))
			{
				userSch_formatDGV_ReportsView(); 

			}
			else if ("Number of Customers".Equals(dd_ReportList.SelectedItem))
			{
				numCx_formatDGV_ReportsView();

			}

		}
		private void apptType_formatDGV_ReportsView(DateTime today)
		{
			
			dd_ByMonth.Visible = true;
			dd_ByType.Visible = true;
			

			if (currMonth == DateTime.MinValue)
			{
				currMonth = currentDate;
			}
			DateTime beginCalDate = new DateTime(currentDate.Year, currMonth.Month, 1);
			DateTime endCalDate = beginCalDate.AddMonths(1).AddDays(-1);
			string beginDate = beginCalDate.ToString("yyyy/MM/dd HH:mm:ss");
			string endDate = endCalDate.ToString("yyyy/MM/dd HH:mm:ss");

			apptTypeGrid.Rows.Clear();
						
			DBControls.ApptTypeReport(apptTypeGrid, beginDate, endDate);
			if (apptTypeGrid.Rows.Count > 0)
			{
				dgv_Reports.DataSource = apptTypeGrid;
				formatapptTypeGridDGV_Report(dgv_Reports);
			}
		}

		private void dd_ByType_SelectedIndexChanged(object sender, EventArgs e)
		{

			if (!("By Type".Equals(dd_ByType.SelectedItem)))
			{
				string selectedType = dd_ByType.SelectedItem.ToString();
				DataView userView = new DataView(apptTypeGrid, $"type = '{selectedType}'", "type Desc", DataViewRowState.CurrentRows);
				dgv_Reports.DataSource = userView;
				formatapptTypeGridDGV_Report(dgv_Reports);
			}
		}
		private void userSch_formatDGV_ReportsView()
		{
			userSchGrid.Rows.Clear();
			DBControls.UserSchReport(userSchGrid);
			dd_ByUser.Visible = true;
			
			if (userSchGrid.Rows.Count > 0)
			{
				dgv_Reports.DataSource = userSchGrid;
				formatuserSchGridDGV_Report(dgv_Reports);
				
				for (int i = 0; i < userSchGrid.Rows.Count; i++)
				{
					object userRowValue = userSchGrid.Rows[i][0];
					if (dd_ByUser.FindStringExact(userRowValue.ToString()) == -1)
					{
						dd_ByUser.Items.Add(userRowValue.ToString());
					}
				}
				
			}
		}

		private void dd_ByUser_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (!("By User".Equals(dd_ByUser.SelectedItem)))
			{
				string selectedName = dd_ByUser.SelectedItem.ToString();
				DataView userView = new DataView(userSchGrid, $"userName = '{selectedName}'", "userName Desc", DataViewRowState.CurrentRows);
				dgv_Reports.DataSource = userView;
				formatuserSchGridDGV_Report(dgv_Reports);
			}
		}
		private void numCx_formatDGV_ReportsView()
		{
			numCxGrid.Rows.Clear();
			numCxGrid.Columns.Clear();
			DBControls.NumCxReport(numCxGrid); 
			if (numCxGrid.Rows.Count > 0)
			{
				int numCount = 0;
				numCount = numCxGrid.Rows.Count;
				numCxGrid.Columns.Add("Total", typeof(int));
				numCxGrid.Rows.Add("Total number of customers", numCount);
				
				dgv_Reports.DataSource = numCxGrid;
				
				formatnumCxGridDGV_Report(dgv_Reports);
			}
		}

		private void formatapptTypeGridDGV_Report(DataGridView e)
		{
			
			e.DefaultCellStyle.BackColor = Color.FromArgb(64, 64, 64);
			e.DefaultCellStyle.ForeColor = Color.SkyBlue;
			e.RowHeadersVisible = false;
			e.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
			
			e.Columns[0].Visible = true;
			e.Columns[1].Visible = true;
			e.Columns[0].HeaderText = "Type";
			e.Columns[1].HeaderText = "Count";
			dgv_Reports.Font = new Font("Microsoft Sans Serif", 8);

			e.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
			e.ReadOnly = true;


		}
		

		private void formatuserSchGridDGV_Report(DataGridView e)
		{

			e.DefaultCellStyle.BackColor = Color.FromArgb(64, 64, 64);
			e.DefaultCellStyle.ForeColor = Color.SkyBlue;
			e.RowHeadersVisible = false;
			e.AutoResizeRows(DataGridViewAutoSizeRowsMode.AllCells);
			e.Columns[0].HeaderText = "User Name";
			e.Columns[1].HeaderText = "User ID"; 
			e.Columns[2].HeaderText = "Type";
			e.Columns[3].HeaderText = "Start Time"; 

			dgv_Reports.Font = new Font("Microsoft Sans Serif", 8);

			e.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
			e.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);

			e.ReadOnly = true;


		}

		private void formatnumCxGridDGV_Report(DataGridView e)
		{
			e.DefaultCellStyle.BackColor = Color.FromArgb(64, 64, 64);
			e.DefaultCellStyle.ForeColor = Color.SkyBlue;
			e.RowHeadersVisible = false;
			e.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
			e.Columns[0].HeaderText = "Customer";
			dgv_Reports.Font = new Font("Microsoft Sans Serif", 8);

			e.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
			e.ReadOnly = true;
		}

		private void dd_ByMonth_SelectedIndexChanged(object sender, EventArgs e)
		{
			dd_ByType.Text = "By Type";
			int byMonth = 0;
			switch (dd_ByMonth.Text)
			{
				case "Jan":
					byMonth = 1;
					break;
				case "Feb":
					byMonth = 2;
					break;
				case "Mar":
					byMonth = 3;
					break;
				case "Apr":
					byMonth = 4;
					break;
				case "May":
					byMonth = 5;
					break;
				case "June":
					byMonth = 6;
					break;
				case "July":
					byMonth = 7;
					break;
				case "Aug":
					byMonth = 8;
					break;
				case "Sept":
					byMonth = 9;
					break;
				case "Oct":
					byMonth = 10;
					break;
				case "Nov":
					byMonth = 11;
					break;
				case "Dec":
					byMonth = 12;
					break;
				default:
					break;
			}

			currMonth = new DateTime(currentDate.Year, byMonth, currentDate.Day);

			if (dd_ByMonth.SelectedIndex > -1)
			{
				apptType_formatDGV_ReportsView(currentDate);
			}

		}

	}
}
