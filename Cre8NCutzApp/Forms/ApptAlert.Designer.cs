﻿
namespace Cre8NCutzApp.Forms
{
	partial class ApptAlert
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.pnl_ApptAlert = new System.Windows.Forms.Panel();
			this.btn_ApptClear = new System.Windows.Forms.Button();
			this.picbox_Siren = new System.Windows.Forms.PictureBox();
			this.lbl_ApptAlert = new System.Windows.Forms.Label();
			this.pnl_ApptAlert.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.picbox_Siren)).BeginInit();
			this.SuspendLayout();
			// 
			// pnl_ApptAlert
			// 
			this.pnl_ApptAlert.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.pnl_ApptAlert.Controls.Add(this.btn_ApptClear);
			this.pnl_ApptAlert.Controls.Add(this.picbox_Siren);
			this.pnl_ApptAlert.Controls.Add(this.lbl_ApptAlert);
			this.pnl_ApptAlert.Location = new System.Drawing.Point(10, 4);
			this.pnl_ApptAlert.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.pnl_ApptAlert.Name = "pnl_ApptAlert";
			this.pnl_ApptAlert.Size = new System.Drawing.Size(952, 198);
			this.pnl_ApptAlert.TabIndex = 83;
			this.pnl_ApptAlert.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnl_ApptAlert_MouseDown);
			this.pnl_ApptAlert.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pnl_ApptAlert_MouseMove);
			this.pnl_ApptAlert.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pnl_ApptAlert_MouseUp);
			// 
			// btn_ApptClear
			// 
			this.btn_ApptClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.btn_ApptClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btn_ApptClear.ForeColor = System.Drawing.Color.SkyBlue;
			this.btn_ApptClear.Location = new System.Drawing.Point(416, 135);
			this.btn_ApptClear.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.btn_ApptClear.Name = "btn_ApptClear";
			this.btn_ApptClear.Size = new System.Drawing.Size(112, 35);
			this.btn_ApptClear.TabIndex = 81;
			this.btn_ApptClear.Text = "Clear";
			this.btn_ApptClear.UseVisualStyleBackColor = false;
			this.btn_ApptClear.Click += new System.EventHandler(this.btn_ApptClear_Click);
			// 
			// picbox_Siren
			// 
			this.picbox_Siren.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.picbox_Siren.Image = global::Cre8NCutzApp.Properties.Resources.alert;
			this.picbox_Siren.Location = new System.Drawing.Point(386, 12);
			this.picbox_Siren.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.picbox_Siren.Name = "picbox_Siren";
			this.picbox_Siren.Size = new System.Drawing.Size(170, 77);
			this.picbox_Siren.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.picbox_Siren.TabIndex = 2;
			this.picbox_Siren.TabStop = false;
			// 
			// lbl_ApptAlert
			// 
			this.lbl_ApptAlert.AutoSize = true;
			this.lbl_ApptAlert.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.lbl_ApptAlert.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbl_ApptAlert.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(63)))), ((int)(((byte)(150)))));
			this.lbl_ApptAlert.Location = new System.Drawing.Point(197, 94);
			this.lbl_ApptAlert.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
			this.lbl_ApptAlert.Name = "lbl_ApptAlert";
			this.lbl_ApptAlert.Size = new System.Drawing.Size(426, 29);
			this.lbl_ApptAlert.TabIndex = 1;
			this.lbl_ApptAlert.Text = "ATTENTION!!! You have a meeting at: ";
			// 
			// ApptAlert
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.ClientSize = new System.Drawing.Size(971, 206);
			this.Controls.Add(this.pnl_ApptAlert);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "ApptAlert";
			this.Text = "ApptAlert";
			this.pnl_ApptAlert.ResumeLayout(false);
			this.pnl_ApptAlert.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.picbox_Siren)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel pnl_ApptAlert;
		private System.Windows.Forms.Button btn_ApptClear;
		private System.Windows.Forms.PictureBox picbox_Siren;
		private System.Windows.Forms.Label lbl_ApptAlert;
	}
}