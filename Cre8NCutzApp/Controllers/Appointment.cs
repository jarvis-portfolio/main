﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.ComponentModel.DataAnnotations;
using Cre8NCutzApp.Models;
//using Microsoft.Ajax.Utilities;
using Twilio;



namespace Cre8NCutzApp.Controllers
{
	public class Appointment
	{
        #region field/prop

        public static int ReminderTime = 30;
        public int Id { get; set; }

        //[Required]
        public string Name { get; set; }

        //[Required, Phone, Display(Name = "Phone number")]
        public string PhoneNumber { get; set; }

        //[Required]
        public DateTime Time { get; set; }

        //[Required]
        public string Timezone { get; set; }

        //[Display(Name = "Created at")]
        public DateTime CreatedAt { get; set; }
        #endregion

        #region cxmethods

        
        #endregion

    }
}
