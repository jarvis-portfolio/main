﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;
using System.Windows.Forms;
using Cre8NCutzApp.Forms;
using Cre8NCutzApp.Models;
using System.IO;
using System.Windows;

namespace Cre8NCutzApp.Controllers
{
	public class DBControls
	{
		public DBControls()
		{ }

		#region fields

		public static DBControls InstDBControls = new DBControls();

		static MySqlConnection sqlConnect = new MySqlConnection(); 
		public MySqlConnection sqlConnection;
		static MySqlCommand sqlCommand = new MySqlCommand(); 
		string sqlQuery;
		public static string constring = DBConnect.OpenDB();
		static DateTime currentTime = DateTime.Now.ToLocalTime();

		//StreamWriter userLoginFileWrite;
		//string userLoginFile = "loginTimeStamp.txt";
		public static int counter = 1;
		UserModel currentUser = new UserModel();
		public static CustomerInfoScrn cxInfoScrn = new CustomerInfoScrn();


		#endregion

		

		#region public methods
		
		public static void OpenUserDB()
		{
			sqlConnect.ConnectionString = DBConnect.OpenDB();
			sqlConnect.Open();
			sqlCommand.Connection = sqlConnect;
		}

		public static void CloseConnection()
		{
			sqlConnect.Close();
		}

		public static void AlterDB()
		{
			OpenUserDB(); //fix this never null
			string altQuery = "Select userNo FROM user"; //WHERE userName='" + InstDBControls.currentUser.UserName + "'";
			MySqlCommand altCmd = new MySqlCommand(altQuery, sqlConnect);
			MySqlDataReader altRead = altCmd.ExecuteReader();
			if (!altRead.Read())
			{
				CloseConnection();
				string path = @"C:\Program Files (x86)\VisionAccentLLC\Cre8NCutz Planner\newClientSch_scr.sql";//@"C:\Users\taylo\source\repos\Cre8NCutzPlanner\Cre8NCutzApp\bin\Release\newClientSch_scr.sql";//"C:\Program Files (x86)\VisionAccentLLC\Cre8NCutz Planner\newClientSch_scr.sql";
				if(path != null)
				{
					OpenUserDB();
					string runDDL = File.ReadAllText(path);
					MySqlCommand cmdDDL = new MySqlCommand(runDDL, sqlConnect);
					cmdDDL.ExecuteNonQuery();
					MessageBox.Show("DB should be updated.");

					sqlConnect.Close();
				}
				else
				{
					MessageBox.Show("Unable to locate file.");
				}
			}
			CloseConnection();
		}
		
		#endregion

#region Login Screen
		public bool IsValidLogin(string user, string pass)
		{
			currentUser.UserName = user;
			currentUser.UserPassword = pass;
			AlterDB();

			OpenUserDB();

			//string addUserNo = "";
			
			sqlQuery = "SELECT * FROM user WHERE username='" + user + "' AND password='" + pass + "'";
			if (sqlQuery == null)
			{
				MessageBox.Show("sqlQuery failed...");
				return false;
			}
			try
			{
				//MySqlCommand cmdUpdate = new MySqlCommand(addUserNo, sqlConnect);
				//cmdUpdate.ExecuteNonQuery();

				MySqlCommand command = new MySqlCommand(sqlQuery, sqlConnect);
				MySqlDataReader reader = command.ExecuteReader();
				if (reader.Read()) 
				{
					
					//FileStream newUserTimeStamp = new FileStream(userLoginFile, FileMode.Append, FileAccess.Write);
					//userLoginFileWrite = new StreamWriter(newUserTimeStamp);
					//userLoginFileWrite.WriteLine($" UserID: {reader.GetInt32(0)}, User: {reader.GetString(1)}, Date/Time of Login: {currentTime}");
					//userLoginFileWrite.Close();
					currentUser.UserID = reader.GetInt32(0);
					reader.Close();
					sqlConnect.Close();

					OpenUserDB();
					string alertQuery = "SELECT start, type, appointment.customerId, customerName" +
							" FROM client_schedule.appointment, client_schedule.customer" +
					" WHERE userId = @userId AND DATEDIFF(@current, start) = 0 AND TIMESTAMPDIFF(MINUTE, start, @current) < 15 AND TIMESTAMPDIFF(MINUTE, start, @current) <= 0"; // start >= DATE_SUB(@current, INTERVAL 15 MINUTE)";
					MySqlCommand cmdGetAlert = new MySqlCommand(alertQuery, sqlConnect);
					cmdGetAlert.Parameters.AddWithValue("@userId", currentUser.UserID); 
					cmdGetAlert.Parameters.AddWithValue("@current", currentTime.ToUniversalTime());
					MySqlDataReader readerAlert = cmdGetAlert.ExecuteReader();
					if (readerAlert.Read())
					{
						DateTime dt_AlertStart = readerAlert.GetDateTime(0).ToLocalTime();
						string alertStart = dt_AlertStart.ToString("hh:mm:ss");
						string alertType = readerAlert.GetString(1);
						int alertCustomerId = readerAlert.GetInt32(2);
						string alertCust = readerAlert.GetString(3);
						if (alertStart != null)
						{
							//MessageBox.Show("Current time " + currentTime.ToString() + " Current time universal "+ currentTime.ToUniversalTime().ToString("HH:mm:ss") + " Reader time universal " + readerAlert.GetDateTime(0).ToString() + " Time for DB " + alertStart + " " + alertCustomerId);
							ApptAlert.AppointmentTime(alertStart);							
							//  - Need to test appointments and make sure its firing correctly.
						}
						AppointmentModel alertModel = new AppointmentModel() { ApptStartTime = dt_AlertStart, ApptType = alertType, CustName = alertCust };
						alertModel.sendSMSApptNotify(alertModel);
					}
					sqlConnect.Close();
					readerAlert.Close();

					return true;

				}
				else
				{
					reader.Close();					
					sqlConnect.Close();

					//FileStream newUserTimeStamp = new FileStream(userLoginFile, FileMode.Append, FileAccess.Write);
					//userLoginFileWrite = new StreamWriter(newUserTimeStamp);
					//userLoginFileWrite.WriteLine($" Failed login attempt, Date/Time of Login: {currentTime}");
					//userLoginFileWrite.Close();
					return false;
				}
			}
			catch (MySqlException ex)
			{
				MessageBox.Show("Unable to open database." + ex.Message);
				return false;
			}

		}

		
			#endregion

#region Customer Info Screen

		

		public static void LoadCustomer(string cxName)
		{
			string selectedSearch = null;
			selectedSearch = cxName;

			MySqlConnection sqlcon = new MySqlConnection(constring);
			MySqlCommand sqlcmd = new MySqlCommand
				("SELECT e.customerID, e.customerName, a.address, a.phone, c.city, d.country" +
				"FROM customer e" +
				"INNER JOIN address a ON e.addressId = a.addressId" +
				"INNER JOIN city c ON a.cityId = c.cityId" +
				"INNER JOIN country d ON c.countryid = d.countryId" +
				"WHERE e.customerName = '" + selectedSearch + "'", sqlcon);


			try
			{
				sqlcon.Open();
				MySqlDataReader myReader2 = sqlcmd.ExecuteReader();
				if (myReader2.Read())
				{
					cxInfoScrn.txt_CxID.Text = myReader2.GetString("customerId");
					cxInfoScrn.txt_CxName.Text = myReader2.GetString("customerName");
					cxInfoScrn.txt_CxAddress.Text = myReader2.GetString("address");
					cxInfoScrn.txt_CxPhone.Text = myReader2.GetString("phone");
					cxInfoScrn.txt_CxCity.Text = myReader2.GetString("city");
					cxInfoScrn.txt_CxCountry.Text = myReader2.GetString("country");
				}
				else
				{
					cxInfoScrn.txt_CxID.Clear();
					cxInfoScrn.txt_CxName.Clear();
					cxInfoScrn.txt_CxAddress.Clear();
					cxInfoScrn.txt_CxPhone.Clear();
					cxInfoScrn.txt_CxCity.Clear();
					cxInfoScrn.txt_CxCountry.Clear();
				}
				sqlcon.Close();
			}
			catch (MySqlException ex)
			{
				MessageBox.Show("ListChangeEvent failed..." + ex.Message);
			}
		}

		public static ComboBox ChooseCustomer(ref ComboBox dd)
		{
			///
			/// This function populates the textboxes from the dropdown
			///

			OpenUserDB();
			string query = "SELECT customerId, customerName, customer.addressId, address, phone, address.cityId, city, city.countryId, country " +
				" FROM client_schedule.customer, client_schedule.address, client_schedule.city, client_schedule.country " +
				" WHERE customer.addressId = address.addressId AND address.cityId = city.cityId AND city.countryId = country.countryId";


			MySqlCommand cmdDatabase = new MySqlCommand(query, sqlConnect);
			MySqlDataReader myReader; 
			try
			{
				myReader = cmdDatabase.ExecuteReader();
				
				if (myReader.HasRows)
				{
					while (myReader.Read())
					{ // - Note* This is creating all my Customer Models
						CustomerModel cm = new CustomerModel {
							CustId = myReader.GetInt32(0),
							CustName = myReader.GetString(1),
							CustAddrId = myReader.GetInt32(2),
							CustAddr = myReader.GetString(3),
							CustAddrPhone = myReader.GetString(4),
							CustCityId = myReader.GetInt32(5),
							CustCityName = myReader.GetString(6),
							CustCountryId = myReader.GetInt32(7),
							CustCountryName = myReader.GetString(8),

						};
						
						dd.Items.Add(cm);
					}
					
				}
				sqlConnect.Close();
				return dd;
			}
			catch (MySqlException ex)
			{
				MessageBox.Show("Choose Customer failed..." + ex.Message);
			}
			
			return dd;
		}

		public static void NewCustomer(CustomerModel newCx)
		{

			OpenUserDB();

			string countryquery = "INSERT INTO client_schedule.country VALUES " +
				"(NULL, @country, @createDate, @createdBy, @lastUpdate, @lastUpdateBy)";

			MySqlCommand cmdDatabase = new MySqlCommand(countryquery, sqlConnect);
			cmdDatabase.Parameters.AddWithValue("@countryId", "NULL"); 
			cmdDatabase.Parameters.AddWithValue("@country", newCx.CustCountryName);
			cmdDatabase.Parameters.AddWithValue("@createDate", currentTime); 
			cmdDatabase.Parameters.AddWithValue("@createdBy", "hrdcdAdmin");
			cmdDatabase.Parameters.AddWithValue("@lastUpdate", currentTime); 
			cmdDatabase.Parameters.AddWithValue("@lastUpdateBy", "hrdcdAdmin"); 
			cmdDatabase.ExecuteNonQuery();

			string getCountryId = "SELECT LAST_INSERT_ID() FROM client_schedule.country";
			MySqlCommand cmdGetCountryId = new MySqlCommand(getCountryId, sqlConnect);
			MySqlDataReader myReaderCountryId;
			try
			{
				myReaderCountryId = cmdGetCountryId.ExecuteReader();
				if (myReaderCountryId.HasRows)
					while (myReaderCountryId.Read())
					{
						newCx.CustCountryId = myReaderCountryId.GetInt32(0);
						
					}
				sqlConnect.Close();
			}
			catch (MySqlException ex)
			{
				MessageBox.Show("Add Country ID failed..." + ex.Message);
			}

			OpenUserDB();
			string cityquery = "INSERT INTO client_schedule.city VALUES " +
				"(NULL, @city, @countryId, @createDate, @createdBy, @lastUpdate, @lastUpdateBy)";
			cmdDatabase = new MySqlCommand(cityquery, sqlConnect);
			cmdDatabase.Parameters.AddWithValue("@cityId", "NULL"); 
			cmdDatabase.Parameters.AddWithValue("@city", newCx.CustCityName);
			cmdDatabase.Parameters.AddWithValue("@countryId", newCx.CustCountryId);  
			cmdDatabase.Parameters.AddWithValue("@createDate", currentTime);
			cmdDatabase.Parameters.AddWithValue("@createdBy", "YourMom");
			cmdDatabase.Parameters.AddWithValue("@lastUpdate", currentTime); 
			cmdDatabase.Parameters.AddWithValue("@lastUpdateBy", "hrdcdAdmin"); 
			cmdDatabase.ExecuteNonQuery();

			string getCityId = "SELECT LAST_INSERT_ID() FROM client_schedule.city";
			MySqlCommand cmdGetCityId = new MySqlCommand(getCityId, sqlConnect);
			MySqlDataReader myReaderCityId;
			try
			{
				myReaderCityId = cmdGetCityId.ExecuteReader();
				if (myReaderCityId.HasRows)
					while (myReaderCityId.Read())
					{
						newCx.CustCityId = myReaderCityId.GetInt32(0);

					}
				sqlConnect.Close();
			}
			catch (MySqlException ex)
			{
				MessageBox.Show("Add CIty ID failed..." + ex.Message);
			}

			OpenUserDB();
			string addressquery = "INSERT INTO client_schedule.address VALUES " +
				"(NULL, @address, @address2, @cityId, @postalCode, @phone, @createDate, @createdBy, @lastUpdate, @lastUpdatedBy)";
			cmdDatabase = new MySqlCommand(addressquery, sqlConnect);
			cmdDatabase.Parameters.AddWithValue("@addressId", "NULL");
			cmdDatabase.Parameters.AddWithValue("@address", newCx.CustAddr);
			cmdDatabase.Parameters.AddWithValue("@address2", "NULL");
			cmdDatabase.Parameters.AddWithValue("@cityId", newCx.CustCityId); 
			cmdDatabase.Parameters.AddWithValue("@postalCode", "NULL");
			cmdDatabase.Parameters.AddWithValue("@phone", newCx.CustAddrPhone);
			cmdDatabase.Parameters.AddWithValue("@createDate", currentTime); 
			cmdDatabase.Parameters.AddWithValue("@createdBy", "hrdcdAdmin"); 
			cmdDatabase.Parameters.AddWithValue("@lastUpdate", currentTime); 
			cmdDatabase.Parameters.AddWithValue("@lastUpdatedBy", "YourMom");
			cmdDatabase.ExecuteNonQuery();

			string getAddressId = "SELECT LAST_INSERT_ID() FROM client_schedule.address";
			MySqlCommand cmdGetAddressId = new MySqlCommand(getAddressId, sqlConnect);
			MySqlDataReader myReaderAddressId;
			try
			{
				myReaderAddressId = cmdGetAddressId.ExecuteReader();
				if (myReaderAddressId.HasRows)
					while (myReaderAddressId.Read())
					{
						newCx.CustAddrId = myReaderAddressId.GetInt32(0);

					}
				sqlConnect.Close();
			}
			catch (MySqlException ex)
			{
				MessageBox.Show("Add Address ID failed..." + ex.Message);
			}

			OpenUserDB();
			string customerquery = "INSERT INTO client_schedule.customer VALUES " +
				"(NULL, @customerName, @addressId, @active, @createDate, @createdBy, @lastUpdate, @lastUpdateBy)";
			cmdDatabase = new MySqlCommand(customerquery, sqlConnect);
			cmdDatabase.Parameters.AddWithValue("@customerId", "NULL"); 
			cmdDatabase.Parameters.AddWithValue("@customerName", newCx.CustName);
			cmdDatabase.Parameters.AddWithValue("@addressId", newCx.CustAddrId); 
			cmdDatabase.Parameters.AddWithValue("@active", 1);
			cmdDatabase.Parameters.AddWithValue("@createDate", currentTime); 
			cmdDatabase.Parameters.AddWithValue("@createdBy", "hrdcdAdmin"); 
			cmdDatabase.Parameters.AddWithValue("@lastUpdate", currentTime);
			cmdDatabase.Parameters.AddWithValue("@lastUpdateBy", "hrdcdAdmin");
			cmdDatabase.ExecuteNonQuery();
			


			sqlConnect.Close();
			MessageBox.Show("New Customer has been created. You can continue.");
		}

		public static void UpdateCustomer(CustomerModel updateCx)
		{
			try
			{
				OpenUserDB();
				string customerquery = "UPDATE client_schedule.customer" +
					" SET customerName = @customerName" +
					" WHERE @customerId = customerId"; 

				MySqlCommand cmdDatabase = new MySqlCommand(customerquery, sqlConnect);
				cmdDatabase.Parameters.AddWithValue("@customerId", updateCx.CustId);
				cmdDatabase.Parameters.AddWithValue("@customerName", updateCx.CustName);
				cmdDatabase.ExecuteNonQuery();
				sqlConnect.Close();


				OpenUserDB();
				string getAddressId = "SELECT addressId FROM client_schedule.customer WHERE customerId = @customerId";
				MySqlCommand cmdGetAddressId = new MySqlCommand(getAddressId, sqlConnect);
				cmdGetAddressId.Parameters.AddWithValue("@customerId", updateCx.CustId);
				Object custAddrId = cmdGetAddressId.ExecuteScalar();
				updateCx.CustAddrId = Convert.ToInt32(custAddrId.ToString());
				sqlConnect.Close();


				OpenUserDB();
				string addressquery = "UPDATE client_schedule.address" +
					" SET address = @address, phone = @phone" +
					" WHERE @addressId = addressId";

				cmdDatabase = new MySqlCommand(addressquery, sqlConnect);
				cmdDatabase.Parameters.AddWithValue("@addressId", updateCx.CustAddrId);
				cmdDatabase.Parameters.AddWithValue("@address", updateCx.CustAddr);
				cmdDatabase.Parameters.AddWithValue("@phone", updateCx.CustAddrPhone);
				cmdDatabase.ExecuteNonQuery();
				sqlConnect.Close();

				OpenUserDB();
				string getCityId = "SELECT cityId FROM client_schedule.address WHERE addressId = @addressId";
				MySqlCommand cmdGetCityId = new MySqlCommand(getCityId, sqlConnect);
				cmdGetCityId.Parameters.AddWithValue("@addressId", updateCx.CustAddrId);
				Object custCityId = cmdGetCityId.ExecuteScalar();
				updateCx.CustCityId = Convert.ToInt32(custCityId.ToString());
				sqlConnect.Close();



				OpenUserDB();
				string cityquery = "UPDATE client_schedule.city" +
					" SET city = @city" +
					" WHERE @cityId = cityId"; 

				cmdDatabase = new MySqlCommand(cityquery, sqlConnect);
				cmdDatabase.Parameters.AddWithValue("@cityId", updateCx.CustCityId);
				cmdDatabase.Parameters.AddWithValue("@city", updateCx.CustCityName);
				cmdDatabase.ExecuteNonQuery();
				sqlConnect.Close();

				OpenUserDB();
				string getCountryId = "SELECT countryId FROM client_schedule.city WHERE cityId = @cityId";
				MySqlCommand cmdGetCountryId = new MySqlCommand(getCountryId, sqlConnect);
				cmdGetCountryId.Parameters.AddWithValue("@cityId", updateCx.CustCityId);
				Object custCountryId = cmdGetCountryId.ExecuteScalar();
				updateCx.CustCountryId = Convert.ToInt32(custCountryId.ToString());
				sqlConnect.Close();


				OpenUserDB();
				string countryquery = "UPDATE client_schedule.country" +
					" SET country = @country" +
					" WHERE @countryId = countryId"; 

				cmdDatabase = new MySqlCommand(countryquery, sqlConnect);
				cmdDatabase.Parameters.AddWithValue("@countryId", updateCx.CustCountryId);  
				cmdDatabase.Parameters.AddWithValue("@country", updateCx.CustCountryName);
				cmdDatabase.ExecuteNonQuery();
				sqlConnect.Close();

				MessageBox.Show("Customer has been updated");
			}
			catch (MySqlException ex)
			{

				MessageBox.Show("Update to customer failed..." + ex.Message);
			}
			finally
			{
				sqlConnect.Close();
			}

			
		}

		public static void DeleteCustomer(CustomerModel deleteCX)
		{
			try
			{
				OpenUserDB();
				string countryquery = "DELETE FROM client_schedule.country WHERE countryId = @countryId";
				MySqlCommand cmdDatabase = new MySqlCommand(countryquery, sqlConnect);
				cmdDatabase.Parameters.AddWithValue("@countryId", deleteCX.CustCountryId);
				cmdDatabase.ExecuteNonQuery();
				sqlConnect.Close();

				OpenUserDB();
				string cityquery = "DELETE FROM client_schedule.city WHERE cityId = @cityId";
				cmdDatabase = new MySqlCommand(cityquery, sqlConnect);
				cmdDatabase.Parameters.AddWithValue("@cityId", deleteCX.CustCityId);
				cmdDatabase.ExecuteNonQuery();
				sqlConnect.Close();

				OpenUserDB();
				string addressquery = "DELETE FROM client_schedule.address WHERE addressId = @addressId";
				cmdDatabase = new MySqlCommand(addressquery, sqlConnect);
				cmdDatabase.Parameters.AddWithValue("@addressId", deleteCX.CustAddrId);
				cmdDatabase.ExecuteNonQuery();
				sqlConnect.Close();

				OpenUserDB();
				string customerquery = "DELETE FROM client_schedule.customer WHERE customerId = @customerId";
				cmdDatabase = new MySqlCommand(customerquery, sqlConnect);
				cmdDatabase.Parameters.AddWithValue("@customerId", deleteCX.CustId);
				cmdDatabase.ExecuteNonQuery();
				sqlConnect.Close();

				MessageBox.Show("Successfully removed customer.");
			}
			catch (MySqlException ex)
			{

				MessageBox.Show("Unable to delete customer..." + ex.Message);
				sqlConnect.Close();
			}

			
		}


#endregion

#region Appointment Screen
		

		public static DataTable ApptGrid(DataTable dataTable, int cxId)
		{
			// Take the value in txt_ApptCxID then search client_schedule.appointment for all matching customerId
			// populate datagrid with Types and start time for each appointment
			// One selection event create anoter function to display the data of that row to the text boxes
			// Then the rest is similar to previous form for add, update and delete
			// Need to get all the data in order to return the model and assign it as a datasource for the dgv

			try
			{
				
				OpenUserDB();
				string dgvCxApptquery = "SELECT appointmentId, customerName, customer.customerId, appointment.userId, type, start" +
					" FROM client_schedule.appointment, client_schedule.customer" +
					" WHERE customer.customerId = appointment.customerId AND customer.customerId = @customerId";
				MySqlCommand cmdAppt = new MySqlCommand(dgvCxApptquery, sqlConnect);
				cmdAppt.Parameters.AddWithValue("@customerId", cxId);
				MySqlDataAdapter adapterAppt = new MySqlDataAdapter(cmdAppt);
				adapterAppt.Fill(dataTable);
			}
			catch (MySqlException ex)
			{
				MessageBox.Show("Unable to load grid..." + ex.Message);
			}
			finally
			{
				sqlConnect.Close();
			}
			for (int i = 0; i < dataTable.Rows.Count; i++)
			{
				dataTable.Rows[i]["start"] = ((DateTime)dataTable.Rows[i]["start"]).ToLocalTime();
			}

			return dataTable;



		}

		public static void NewAppointment(AppointmentModel newAppt)
		{
			

			OpenUserDB();
			newAppt.UserID = InstDBControls.currentUser.UserID;
			string checkStartquery = "SELECT COUNT(*) FROM client_schedule.appointment WHERE start = @start AND userId = @userId";
			MySqlCommand cmdDatabase = new MySqlCommand(checkStartquery, sqlConnect);
			cmdDatabase.Parameters.AddWithValue("@start", newAppt.ApptStartTime);
			cmdDatabase.Parameters.AddWithValue("@userId", newAppt.UserID);
			int apptExist = Convert.ToInt32(cmdDatabase.ExecuteScalar());
			sqlConnect.Close();
			//MessageBox.Show(apptExist.ToString() + newAppt.UserID);

			if (apptExist > 0)
			{
				MessageBox.Show("Unable to add new appointment. Appointment already exist for this time slot.");
				return;
			}
			
				

			OpenUserDB();
			string appointmentquery = "INSERT INTO client_schedule.appointment VALUES " +
				"(NULL, @customerId, @userId, @title, @description, @location, @contact, @type, @url, @start, @end, @createDate, @createdBy, @lastUpdate, @lastUpdateBy)";
			cmdDatabase = new MySqlCommand(appointmentquery, sqlConnect);
			cmdDatabase.Parameters.AddWithValue("@appointmentId", "NULL");
			cmdDatabase.Parameters.AddWithValue("@customerId", newAppt.CustId);
			cmdDatabase.Parameters.AddWithValue("@userId", newAppt.UserID);
			cmdDatabase.Parameters.AddWithValue("@title", "New Appointment");
			cmdDatabase.Parameters.AddWithValue("@description", "Description of Appointment");
			cmdDatabase.Parameters.AddWithValue("@location", "Zoom/Teams");
			cmdDatabase.Parameters.AddWithValue("@contact", "phone");
			cmdDatabase.Parameters.AddWithValue("@type", newAppt.ApptType);
			cmdDatabase.Parameters.AddWithValue("@url", "www.zoom.com...");
			cmdDatabase.Parameters.AddWithValue("@start", newAppt.ApptStartTime);
			cmdDatabase.Parameters.AddWithValue("@end", currentTime);
			cmdDatabase.Parameters.AddWithValue("@createDate", currentTime);
			cmdDatabase.Parameters.AddWithValue("@createdBy", "hrdcdAdmin");
			cmdDatabase.Parameters.AddWithValue("@lastUpdate", currentTime);
			cmdDatabase.Parameters.AddWithValue("@lastUpdateBy", "hrdcdAdmin");
			cmdDatabase.ExecuteNonQuery();

			sqlConnect.Close();
			MessageBox.Show("New Appointment has been created. You can continue.");
			

		}

		public static void UpdateAppointment(AppointmentModel updateAppt)
		{
			try
			{
				OpenUserDB();
				string appointmentquery = "UPDATE client_schedule.appointment" +
					" SET type = @type, start = @start" +
					" WHERE @appointmentId = appointmentId";

				MySqlCommand cmdDatabase = new MySqlCommand(appointmentquery, sqlConnect);
				cmdDatabase.Parameters.AddWithValue("@appointmentId", updateAppt.ApptID);
				cmdDatabase.Parameters.AddWithValue("@type", updateAppt.ApptType);
				cmdDatabase.Parameters.AddWithValue("@start", updateAppt.ApptStartTime);
				cmdDatabase.ExecuteNonQuery();
				//sqlConnect.Close();

				MessageBox.Show("Appointment has been updated");
			}
			catch (MySqlException ex)
			{
				MessageBox.Show("Failed to update appointment..." + ex.Message);
			}
			finally
			{
				sqlConnect.Close();
			}

		}

		public static void DeleteAppointment(AppointmentModel deleteAppt)
		{
			OpenUserDB();
			string appointmentquery = "DELETE FROM client_schedule.appointment WHERE appointmentId = @appointmentId";
			MySqlCommand cmdDatabase = new MySqlCommand(appointmentquery, sqlConnect);
			cmdDatabase.Parameters.AddWithValue("@appointmentId", deleteAppt.ApptID);
			cmdDatabase.ExecuteNonQuery();
			sqlConnect.Close();

			MessageBox.Show("Successfully removed appointment.");
		}
		#endregion

#region Calendar Screen
		public static DataTable WeeklyCalendar(DataTable dataTable, string beginDate, string endDate)
		{

			try
			{
				OpenUserDB();
				string dgvWeeklyquery = "SELECT appointmentId, customerName, customer.customerId, appointment.userId, type, start" +
					" FROM client_schedule.appointment, client_schedule.customer" +
					" WHERE customer.customerId = appointment.customerId AND start BETWEEN '" + beginDate + "' AND '" + endDate + "'";
				MySqlCommand cmdAppt = new MySqlCommand(dgvWeeklyquery, sqlConnect);
				MySqlDataAdapter adapterAppt = new MySqlDataAdapter(cmdAppt);
				adapterAppt.Fill(dataTable);
			}
			catch (MySqlException ex)
			{
				MessageBox.Show("Unable to load grid..." + ex.Message);
			}
			finally
			{
				sqlConnect.Close();
			}
			for (int i = 0; i < dataTable.Rows.Count; i++)
			{
				dataTable.Rows[i]["start"] = ((DateTime)dataTable.Rows[i]["start"]).ToLocalTime();
			}

			return dataTable;
		}

		public static DataTable MonthlyCalendar(DataTable dataTable, string beginDate, string endDate)
		{
			try
			{
				
				OpenUserDB();
				string dgvMonthlyquery = "SELECT appointmentId, customerName, customer.customerId, appointment.userId, type, start" +
					" FROM client_schedule.appointment, client_schedule.customer" +
					" WHERE customer.customerId = appointment.customerId AND start BETWEEN '" + beginDate + "' AND '" + endDate + "'";
				MySqlCommand cmdAppt = new MySqlCommand(dgvMonthlyquery, sqlConnect);
				MySqlDataAdapter adapterAppt = new MySqlDataAdapter(cmdAppt);
				adapterAppt.Fill(dataTable);
			}
			catch (MySqlException ex)
			{
				MessageBox.Show("Unable to load grid..." + ex.Message);
			}
			finally
			{
				sqlConnect.Close();
			}
			for (int i = 0; i < dataTable.Rows.Count; i++)
			{
				dataTable.Rows[i]["start"] = ((DateTime)dataTable.Rows[i]["start"]).ToLocalTime();
			}

			return dataTable;
		}

		#endregion

#region Report Screen

		public static DataTable ApptTypeReport(DataTable dataTable, string beginDate, string endDate)
		{
			
			try
			{
				
				OpenUserDB();
				string apptTypequery = "SELECT type, COUNT(*)" + 
					" FROM client_schedule.appointment" +
					" WHERE start BETWEEN '" + beginDate + "' AND '" + endDate + "'" +
					" GROUP BY type";
				MySqlCommand cmdAppt = new MySqlCommand(apptTypequery, sqlConnect);
				MySqlDataAdapter adapterAppt = new MySqlDataAdapter(cmdAppt);
				adapterAppt.Fill(dataTable);
				return dataTable;
			}
			catch (MySqlException ex)
			{
				MessageBox.Show("Unable to load grid..." + ex.Message);
			}
			finally
			{
				sqlConnect.Close();
			}
			
			return dataTable;
		}

		public static DataTable UserSchReport(DataTable dataTable)
		{
			//int uid = InstDBControls.currentUser.UserID;
			try
			{
				OpenUserDB();
				string apptTypequery = "SELECT userName, user.userId, type, start" +
					" FROM client_schedule.appointment, client_schedule.user" +
					//" WHERE user.userId = appointment.userId AND user.userId = @userId";
					" WHERE user.userId = appointment.userId";

				MySqlCommand cmdAppt = new MySqlCommand(apptTypequery, sqlConnect);
				//cmdAppt.Parameters.AddWithValue("@userId", uid);
				MySqlDataAdapter adapterAppt = new MySqlDataAdapter(cmdAppt);
				adapterAppt.Fill(dataTable);
				
			}
			catch (MySqlException ex)
			{
				MessageBox.Show("Unable to load grid..." + ex.Message);
			}
			finally
			{
				sqlConnect.Close();
			}
			for (int i = 0; i < dataTable.Rows.Count; i++)
			{
				dataTable.Rows[i]["start"] = ((DateTime)dataTable.Rows[i]["start"]).ToLocalTime();
			}
			return dataTable;
		}

		public static DataTable NumCxReport(DataTable dataTable)
		{
			try
			{
				OpenUserDB();
				string apptTypequery = "SELECT customerName" +
					" FROM client_schedule.customer";

				MySqlCommand cmdAppt = new MySqlCommand(apptTypequery, sqlConnect);
				MySqlDataAdapter adapterAppt = new MySqlDataAdapter(cmdAppt);
				adapterAppt.Fill(dataTable);
				return dataTable;
			}
			catch (MySqlException ex)
			{
				MessageBox.Show("Unable to load grid..." + ex.Message);
			}
			finally
			{
				sqlConnect.Close();
			}

			return dataTable;
		}
		#endregion

#region Number Screen
		public static string UserNoGrid()
		{
			string userNumber = "";
			try
			{
				OpenUserDB();
				int thisID = InstDBControls.currentUser.UserID;
				
				string getUserquery = "SELECT userName, userNo FROM client_schedule.user WHERE userId = @userId";
				MySqlCommand cmdDatabase = new MySqlCommand(getUserquery, sqlConnect);
				cmdDatabase.Parameters.AddWithValue("@userId", thisID);
				MySqlDataReader readerUserNo;
				readerUserNo = cmdDatabase.ExecuteReader();
				if (readerUserNo.HasRows)
				{
					while (readerUserNo.Read())
					{
						userNumber = readerUserNo.GetString(1);
					}
				}				
			}
			catch (MySqlException ex)
			{
				MessageBox.Show("Unable to retrieve..." + ex.Message);
			}
			finally
			{
				sqlConnect.Close();
			}

			return userNumber;
		}

		public static void UpdateUserNo(string userNo)
		{
			try
			{
				OpenUserDB();
				int thisID = InstDBControls.currentUser.UserID;
				string userNoquery = "UPDATE client_schedule.user SET userNo = @userNo WHERE @userId = userId";

				MySqlCommand cmdDatabase = new MySqlCommand(userNoquery, sqlConnect);
				cmdDatabase.Parameters.AddWithValue("@userId", thisID);
				cmdDatabase.Parameters.AddWithValue("@userNo", userNo);
				cmdDatabase.ExecuteNonQuery();
				MessageBox.Show("Your phone number has been updated");
			}
			catch (MySqlException ex)
			{
				MessageBox.Show("Failed to update phone number..." + ex.Message + ex.Data + ex.Code);
			}
			finally
			{
				sqlConnect.Close();
			}

		}
				
	}

	#endregion

}
