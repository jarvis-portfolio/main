﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using System.Globalization;
using System.Text;
using System.IO;
using Cre8NCutzApp.Models;
using System.Text.Json;
using Twilio;

namespace Cre8NCutzApp
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{

			CultureInfo userLocation = CultureInfo.CurrentCulture;
			if (userLocation.TwoLetterISOLanguageName != "en")
			{
				Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("fr");
			}
			
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(new LoginScrn());

			var twilioConfigString = File.ReadAllText("twilioconfig.json");
			var twilioConfig = JsonSerializer.Deserialize<TwilioConfig>(twilioConfigString);
			TwilioClient.Init(twilioConfig.AccountSID, twilioConfig.AuthToken);

		}
	}
}
